package com.upc.tfm.weoffer.listchats

data class ListChatsRequest (
    val id: String,
    val user_id: String,
    val user_name: String,
    val user_surname: String,
    val user_image: String,
    val date: String,
    val time: String,
    var last_message: String
)