package com.upc.tfm.weoffer.createoffer

import com.upc.tfm.weoffer.offerdetail.ImageRequest

data class CreateOfferRequest (
    val title: String,
    val description: String,
    val online: Boolean,
    val location: String,
    val hour_price: Float,
    val total_price: Float,
    val images: ArrayList<ImageRequest?>?
)