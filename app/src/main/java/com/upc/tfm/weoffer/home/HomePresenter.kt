package com.upc.tfm.weoffer.home

import android.content.Context
import android.content.Intent
import android.view.MenuItem
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.google.android.material.navigation.NavigationView
import com.upc.tfm.weoffer.R
import com.upc.tfm.weoffer.listchats.ListChatsView
import com.upc.tfm.weoffer.login.LoginView
import com.upc.tfm.weoffer.myoffers.MyOffersView
import com.upc.tfm.weoffer.offers.OffersView
import com.upc.tfm.weoffer.profile.ProfileView
import com.upc.tfm.weoffer.storage.InternalStorage
import com.upc.tfm.weoffer.utils.UtilOffer
import com.upc.tfm.weoffer.utils.UtilUser

class HomePresenter(val context: Context, val view: IHomeView): NavigationView.OnNavigationItemSelectedListener {

    private lateinit var currentFragment: Fragment
    private var internalStorage: InternalStorage = InternalStorage()

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_user_profile -> {
                if (!item.isChecked) {
                    item.isChecked = true
                    currentFragment = ProfileView()
                    (context as FragmentActivity).supportFragmentManager
                        .beginTransaction()
                        .setCustomAnimations(R.animator.fade_in, R.animator.fade_out)
                        .replace(R.id.home_frameFragment, currentFragment)
                        .commit()
                    view.setTitle(R.string.profile_title)
                }
            }
            R.id.nav_offers -> {
                if (!item.isChecked) {
                    item.isChecked = true
                    currentFragment = OffersView()
                    (context as FragmentActivity).supportFragmentManager
                        .beginTransaction()
                        .setCustomAnimations(R.animator.fade_in, R.animator.fade_out)
                        .replace(R.id.home_frameFragment, currentFragment)
                        .commit()
                    view.setTitle(R.string.home_title)
                }
            }
            R.id.nav_my_offers -> {
                if (!item.isChecked) {
                    item.isChecked = true
                    currentFragment = MyOffersView()
                    (context as FragmentActivity).supportFragmentManager
                        .beginTransaction()
                        .setCustomAnimations(R.animator.fade_in, R.animator.fade_out)
                        .replace(R.id.home_frameFragment, currentFragment)
                        .commit()
                    view.setTitle(R.string.my_offers_title)
                }
            }
            R.id.nav_user_chats -> {
                if (!item.isChecked) {
                    item.isChecked = true
                    currentFragment = ListChatsView()
                    (context as FragmentActivity).supportFragmentManager
                        .beginTransaction()
                        .setCustomAnimations(R.animator.fade_in, R.animator.fade_out)
                        .replace(R.id.home_frameFragment, currentFragment)
                        .commit()
                    view.setTitle(R.string.list_chat_title)
                }
            }
            R.id.nav_logout -> {
                internalStorage.deleteUser(context)
                UtilUser.delete()
                UtilOffer.delete()
                val intent = Intent()
                intent.setClass(context, LoginView::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                context.startActivity(intent)
            }
        }
        view.closeDrawable()
        return true
    }

    fun initUserInfo(userInfo: ArrayList<String>?) {
        if (userInfo != null)
            UtilUser.setData(userInfo)
    }

}