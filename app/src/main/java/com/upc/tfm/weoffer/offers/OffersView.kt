package com.upc.tfm.weoffer.offers

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.upc.tfm.weoffer.R
import com.upc.tfm.weoffer.myoffers.OnItemClickListener
import com.upc.tfm.weoffer.myoffers.addOnItemClickListener
import com.upc.tfm.weoffer.utils.UtilOffer
import kotlinx.android.synthetic.main.my_offers_view.empty_view
import kotlinx.android.synthetic.main.offers_view.*


class OffersView: Fragment(), IOffersView, SwipeRefreshLayout.OnRefreshListener {

    private lateinit var presenter: OffersPresenter
    private lateinit var activity: Activity
    private lateinit var adapter: OffersAdapter
    private var dialog: Dialog? = null
    private var first: Boolean = true


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.offers_view, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)
        (activity as AppCompatActivity).supportActionBar?.title = activity.getString(R.string.home_title)
        init()
    }

    override fun onResume() {
        super.onResume()
        if (!first) {
            presenter.getOffers()
        }
        first = false
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu?.clear()
        inflater?.inflate(R.menu.main_drawer, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    private fun init() {
        presenter = OffersPresenter(activity, this)
        initRecyclerView()
        presenter.getOffers()
        initSearchButton()
        initRefreshLayout()
    }

    private fun initRecyclerView() {
        offers_recyclerView.layoutManager = LinearLayoutManager(context)
        var list: List<OfferRequest> = ArrayList()
        if (UtilOffer.filterOffers != null) {
            if (UtilOffer.filterOffers!!.data != null)
                list = UtilOffer.filterOffers!!.data!!
        }

        adapter = OffersAdapter(context!!, list, presenter)
        offers_recyclerView.adapter = adapter
        emptyRecyclerLogic(list)
        offers_recyclerView.addOnItemClickListener(object : OnItemClickListener {
            override fun onItemClicked(position: Int, view: View) {
                presenter.itemClicked(position)
            }
        })
        offers_recyclerView.addOnScrollListener(object: RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (!offers_recyclerView.canScrollVertically(1)) {
                    presenter.bottomReached()
                }
            }
        })
    }

    private fun emptyRecyclerLogic(myOffers: List<OfferRequest>) {
        if (myOffers.isEmpty()) {
            offers_recyclerView.visibility = View.GONE
            empty_view.visibility = View.VISIBLE
        } else {
            offers_recyclerView.visibility = View.VISIBLE
            empty_view.visibility = View.GONE
        }
    }

    private fun initSearchButton() {
        offers_search_button.setOnClickListener {
            presenter.searchButtonPressed()
        }
    }

    private fun initRefreshLayout() {
        offers_reloadView.setOnRefreshListener(this)
        offers_reloadView.setColorSchemeResources(R.color.colorPrimary)
    }

    // Override

    override fun onAttach(context: Context) {
        super.onAttach(context)
        activity = context as (Activity)
    }

    override fun showProgressDialog() {
        if (context != null) {
            dialog = Dialog(context!!)
            dialog?.setContentView(R.layout.progressbar_layout)
            dialog?.setCancelable(false)
            dialog?.setCanceledOnTouchOutside(false)
            dialog?.show()
        }
    }

    override fun closeDialog() {
        dialog?.dismiss()
    }

    override fun setOffers(data: List<OfferRequest>) {
        //initRecyclerView(data)
    }

    override fun updateOffers(data: List<OfferRequest>?) {
        if (data != null) {
            adapter.updateData(data)
        }
    }

    override fun checkEmpty(list: List<OfferRequest>?) {
        if (list != null) {
            emptyRecyclerLogic(list)
        }
    }

    override fun onRefresh() {
        presenter.getOffers()
        offers_reloadView.isRefreshing = false
    }

}