package com.upc.tfm.weoffer.myoffers

import java.io.Serializable

data class MyOfferRequest (
    val id: String,
    val title: String,
    val hour_price: String,
    val total_price: String,
    val image: String?
): Serializable