package com.upc.tfm.weoffer.profile

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.upc.tfm.weoffer.profile.offers.ProfileOffersFragment
import com.upc.tfm.weoffer.profile.reviews.ProfileReviewsFragment

class ProfilePagerAdapter(fm: FragmentManager, reviewsData: Bundle, offersData: Bundle,
                          fragment_reviews: ProfileReviewsFragment, fragment_offers: ProfileOffersFragment) : FragmentPagerAdapter(fm) {

    private var reviewsBundle: Bundle = reviewsData
    private var offersBundle: Bundle = offersData

    private val fragmentReviews: Fragment = fragment_reviews
    private val fragmentOffers: Fragment = fragment_offers


    override fun getItem(position: Int): Fragment {

        when (position) {
            0 -> {
                fragmentReviews.arguments = reviewsBundle
                return fragmentReviews
            }
            else -> {
                fragmentOffers.arguments = offersBundle
                return fragmentOffers
            }
        }

    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence {

        return when (position) {
            0 -> "Reviews"
            else -> "Offers"
        }
    }

}