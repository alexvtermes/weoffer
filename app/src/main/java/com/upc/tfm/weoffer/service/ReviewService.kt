package com.upc.tfm.weoffer.service
import com.upc.tfm.weoffer.network.*
import com.upc.tfm.weoffer.rateuser.ReviewRequest
import com.upc.tfm.weoffer.rateuser.ReviewResponse
import com.upc.tfm.weoffer.utils.UtilService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ReviewService(private val delegate: ReviewService.OnReviewCompleted) {

    interface OnReviewCompleted {
        fun onReviewCreatedCompleted(code: Int)
        fun onGetReviewsCompleted(response: Pair<Int, ReviewsResponse?>)
        fun onCheckReviewCompleted(response: Pair<Int, CheckReviewResponse?>)
        fun onDeleteReviewCompleted(code: Int)
        fun onGetMyReviewCompleted(response: Pair<Int, MyReviewResponse?>)
    }

    private var callbackCreateReview: Call<Unit>? = null
    private var callbackGetReviews: Call<ReviewsResponse>? = null
    private var callbackCheckReview: Call<CheckReviewResponse>? = null
    private var callbackDelete: Call<Unit>? = null
    private var callbackMyReview: Call<MyReviewResponse>? = null


    fun createReview(request: ReviewRequest, token: String) {
        val retrofit = UtilService.prepareRetrofit()
        val service = retrofit.create(ReviewAPI::class.java)
        UtilService.setPolicy()
        val header: MutableMap<String, String> = HashMap()
        header["Authorization"] = token
        callbackCreateReview = service.createReview(header, request)
        callbackCreateReview?.enqueue(object : Callback<Unit> {
            override fun onFailure(call: Call<Unit>?, t: Throwable) {
                if (call != null && call.isCanceled) {
                } else if (t is Exception) {
                    callbackCreateReview = null
                    delegate.onReviewCreatedCompleted(900)
                }
                callbackCreateReview = null
            }

            override fun onResponse(call: Call<Unit>, response: Response<Unit>?) {
                if (response != null)
                    delegate.onReviewCreatedCompleted(response.code())
                callbackCreateReview = null

            }
        })
    }

    fun getReviews(token: String, id: String) {
        val retrofit = UtilService.prepareRetrofit()
        val service = retrofit.create(ReviewAPI::class.java)
        UtilService.setPolicy()
        val header: MutableMap<String, String> = HashMap()
        header["Authorization"] = token
        val url = "/reviews/" + id
        callbackGetReviews = service.getReviews(url, header)
        callbackGetReviews?.enqueue(object: Callback<ReviewsResponse>
        {
            override fun onFailure(call: Call<ReviewsResponse>?, t: Throwable) {
                if (call != null && call.isCanceled)
                {}
                else if (t is Exception)
                {
                    callbackGetReviews = null
                    delegate.onGetReviewsCompleted(Pair(900, null))
                }
                callbackGetReviews = null
            }

            override fun onResponse(call: Call<ReviewsResponse>, response: Response<ReviewsResponse>?) {
                if (response != null)
                    delegate.onGetReviewsCompleted(Pair(response.code(), response.body()))
                callbackGetReviews = null
            }
        })
    }

    fun checkReview(token: String, id: String) {
        val retrofit = UtilService.prepareRetrofit()
        val service = retrofit.create(ReviewAPI::class.java)
        UtilService.setPolicy()
        val header: MutableMap<String, String> = HashMap()
        header["Authorization"] = token
        val url = "/review/" + id
        callbackCheckReview = service.checkReview(url, header)
        callbackCheckReview?.enqueue(object: Callback<CheckReviewResponse>
        {
            override fun onFailure(call: Call<CheckReviewResponse>?, t: Throwable) {
                if (call != null && call.isCanceled)
                {}
                else if (t is Exception)
                {
                    callbackCheckReview = null
                    delegate.onCheckReviewCompleted(Pair(900, null))
                }
                callbackCheckReview = null
            }

            override fun onResponse(call: Call<CheckReviewResponse>, response: Response<CheckReviewResponse>?) {
                if (response != null)
                    delegate.onCheckReviewCompleted(Pair(response.code(), response.body()))
                callbackCheckReview = null
            }
        })
    }

    fun deleteReview(token: String, userId: String) {
        val retrofit = UtilService.prepareRetrofit()
        val service = retrofit.create(ReviewAPI::class.java)
        UtilService.setPolicy()
        val header: MutableMap<String, String> = HashMap()
        header["Authorization"] = token
        val url = "/review/" + userId
        callbackDelete = service.deleteReview(url, header)
        callbackDelete?.enqueue(object: Callback<Unit>
        {
            override fun onFailure(call: Call<Unit>?, t: Throwable) {
                if (call != null && call.isCanceled)
                {
                    //do nothing view does no exists anymore
                }
                else if (t is Exception)
                {
                    callbackDelete = null
                    delegate.onDeleteReviewCompleted(900)
                }
                callbackDelete = null
            }

            override fun onResponse(call: Call<Unit>, response: Response<Unit>?) {
                if (response != null)
                    delegate.onDeleteReviewCompleted(response.code())
                callbackDelete = null
            }
        })
    }

    fun getMyReviews(token: String) {
        val retrofit = UtilService.prepareRetrofit()
        val service = retrofit.create(ReviewAPI::class.java)
        UtilService.setPolicy()
        val header: MutableMap<String, String> = HashMap()
        header["Authorization"] = token
        callbackMyReview = service.getMyReviews(header)
        callbackMyReview?.enqueue(object: Callback<MyReviewResponse>
        {
            override fun onFailure(call: Call<MyReviewResponse>?, t: Throwable) {
                if (call != null && call.isCanceled)
                {}
                else if (t is Exception)
                {
                    callbackMyReview = null
                    delegate.onGetMyReviewCompleted(Pair(900, null))
                }
                callbackMyReview = null
            }

            override fun onResponse(call: Call<MyReviewResponse>, response: Response<MyReviewResponse>?) {
                if (response != null)
                    delegate.onGetMyReviewCompleted(Pair(response.code(), response.body()))
                callbackMyReview = null
            }
        })
    }

}