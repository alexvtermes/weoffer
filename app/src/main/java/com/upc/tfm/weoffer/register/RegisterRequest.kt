package com.upc.tfm.weoffer.register

data class RegisterRequest (
    val name: String,
    val surname: String,
    val gender: String,
    val birthday: String,
    val email: String,
    val country: String,
    var password: String,
    var image: String
)