package com.upc.tfm.weoffer.service

import com.upc.tfm.weoffer.createoffer.CreateOfferRequest
import com.upc.tfm.weoffer.login.LoginRequest
import com.upc.tfm.weoffer.login.LoginResponse
import com.upc.tfm.weoffer.login.ProfileImageRequest
import com.upc.tfm.weoffer.network.OfferAPI
import com.upc.tfm.weoffer.network.UserAPI
import com.upc.tfm.weoffer.network.UserResponse
import com.upc.tfm.weoffer.register.RegisterRequest
import com.upc.tfm.weoffer.utils.UtilService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class UserService(private val delegate: OnUserCompleted) {

    interface OnUserCompleted {
        fun onLoginCompleted(response: Pair<Int, LoginResponse?>)
        fun onRegisterCompleted(code: Int)
        fun onImageUpdated(code: Int)
        fun onOtherUserInfoCompleted(response: Pair<Int, LoginResponse?>)
    }

    private var callbackLogin: Call<UserResponse>? = null
    private var callbackRegister: Call<Unit>? = null
    private var callbackImage: Call<Unit>? = null
    private var callbackOtherUserInfo: Call<UserResponse>? = null


    fun register(request: RegisterRequest) {
        val retrofit = UtilService.prepareRetrofit()
        val service = retrofit.create(UserAPI::class.java)
        UtilService.setPolicy()
        callbackRegister = service.register(request)
        callbackRegister?.enqueue(object: Callback<Unit> {
            override fun onFailure(call: Call<Unit>?, t: Throwable) {
                if (call != null && call.isCanceled) {
                    //do nothing view does no exists anymore
                } else if (t is Exception) {
                    callbackRegister = null
                    delegate.onRegisterCompleted(900)
                }
                callbackRegister = null
            }

            override fun onResponse(call: Call<Unit>, response: Response<Unit>?) {
                if (response != null)
                    delegate.onRegisterCompleted(response.code())
                callbackRegister = null

            }
        })
    }

    fun doLogin(request: LoginRequest) {
        val retrofit = UtilService.prepareRetrofit()
        val service = retrofit.create(UserAPI::class.java)
        UtilService.setPolicy()
        callbackLogin = service.login(request)
        callbackLogin?.enqueue(object: Callback<UserResponse>
        {
            override fun onFailure(call: Call<UserResponse>?, t: Throwable) {
                if (call != null && call.isCanceled) {}
                else if (t is Exception)
                {
                    callbackLogin = null
                    delegate.onLoginCompleted(Pair(900, null))
                }
                callbackLogin = null
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>?) {
                if (response != null) {
                    if (response.body() != null)
                        delegate.onLoginCompleted(Pair(response.code(), response.body()!!.data))
                    else
                        delegate.onLoginCompleted(Pair(response.code(), null))
                }
                callbackLogin = null
            }
        })
    }

    fun updateProfileImage(request: ProfileImageRequest, token: String) {
        val retrofit = UtilService.prepareRetrofit()
        val service = retrofit.create(UserAPI::class.java)
        UtilService.setPolicy()
        val header: MutableMap<String, String> = HashMap()
        header["Authorization"] = token
        callbackImage = service.updateImage(header, request)
        callbackImage?.enqueue(object: Callback<Unit> {
            override fun onFailure(call: Call<Unit>?, t: Throwable) {
                if (call != null && call.isCanceled) {
                } else if (t is Exception) {
                    callbackImage = null
                    delegate.onImageUpdated(900)
                }
                callbackImage = null
            }

            override fun onResponse(call: Call<Unit>, response: Response<Unit>?) {
                if (response != null)
                    delegate.onImageUpdated(response.code())
                callbackImage = null

            }
        })
    }

    fun getOtherUserInfo(token: String, userId: String) {
        val retrofit = UtilService.prepareRetrofit()
        val service = retrofit.create(UserAPI::class.java)
        UtilService.setPolicy()
        val header: MutableMap<String, String> = HashMap()
        header["Authorization"] = token
        val url = "/user/" + userId
        callbackOtherUserInfo = service.getOtherUserInfo(url, header)
        callbackOtherUserInfo?.enqueue(object: Callback<UserResponse>
        {
            override fun onFailure(call: Call<UserResponse>?, t: Throwable) {
                if (call != null && call.isCanceled)
                {
                    //do nothing view does not exist anymore
                }
                else if (t is Exception)
                {
                    callbackOtherUserInfo = null
                    delegate.onOtherUserInfoCompleted(Pair(900, null))
                }
                callbackOtherUserInfo = null
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>?) {
                if (response != null) {
                    if (response.body() != null)
                        delegate.onOtherUserInfoCompleted(Pair(response.code(), response.body()!!.data))
                    else
                        delegate.onOtherUserInfoCompleted(Pair(response.code(), null))
                }
                callbackOtherUserInfo = null
            }
        })
    }


}