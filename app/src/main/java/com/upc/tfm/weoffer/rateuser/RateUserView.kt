package com.upc.tfm.weoffer.rateuser

import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.content.res.Configuration
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.upc.tfm.weoffer.R
import kotlinx.android.synthetic.main.offer_detail_view.*
import kotlinx.android.synthetic.main.rate_user_view.*

class RateUserView: AppCompatActivity(), IRateUserView {

    private var presenter: RateUserPresenter = RateUserPresenter(this, this)
    private var dialog: Dialog? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.rate_user_view)
        init()
    }

    private fun init() {
        initToolbar()
        initUserId()
        initStars()
        initRateButton()
        initDeleteButton()
        initData()
    }

    private fun initRateButton() {
        rate_user_button.setOnClickListener {
            presenter.rateButtonPressed(
                message = rate_user_message.text.toString()
            )
        }
    }

    private fun initToolbar() {
        setSupportActionBar(app_toolbar)
        if (supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        }
    }

    private fun initDeleteButton() {
        review_delete_button.setOnClickListener {
            presenter.deleteButtonPressed()
        }
    }

    private fun initUserId() {
        val userId = intent.getStringExtra("id")
        presenter.setUserId(userId)
    }

    private fun initStars() {
        rate_user_star_1.setOnClickListener {
            presenter.star1Pressed()
        }
        rate_user_star_2.setOnClickListener {
            presenter.star2Pressed()
        }
        rate_user_star_3.setOnClickListener {
            presenter.star3Pressed()
        }
        rate_user_star_4.setOnClickListener {
            presenter.star4Pressed()
        }
        rate_user_star_5.setOnClickListener {
            presenter.star5Pressed()
        }
    }

    private fun initData() {
        review_delete_button.visibility = View.GONE
        presenter.checkReview()
    }

    // Override

    override fun showStarsError() {
        Toast.makeText(this, R.string.rate_user_stars_error, Toast.LENGTH_SHORT).show()
    }

    override fun showProgressDialog() {
        dialog = Dialog(this)
        dialog?.setContentView(R.layout.progressbar_layout)
        dialog?.setCancelable(false)
        dialog?.setCanceledOnTouchOutside(false)
        dialog?.show()
    }

    override fun closeDialog() {
        dialog?.dismiss()
    }

    override fun showReviewCorrectly() {
        Toast.makeText(this, R.string.rate_user_correctly, Toast.LENGTH_SHORT).show()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return super.onOptionsItemSelected(item!!)
    }

    override fun showStars(stars: Int) {
        when (stars) {
            5 -> {
                rate_user_star_1.setBackgroundResource(R.drawable.ic_star_full_2)
                rate_user_star_2.setBackgroundResource(R.drawable.ic_star_full_2)
                rate_user_star_3.setBackgroundResource(R.drawable.ic_star_full_2)
                rate_user_star_4.setBackgroundResource(R.drawable.ic_star_full_2)
                rate_user_star_5.setBackgroundResource(R.drawable.ic_star_full_2)
            }
            4 -> {
                rate_user_star_1.setBackgroundResource(R.drawable.ic_star_full_2)
                rate_user_star_2.setBackgroundResource(R.drawable.ic_star_full_2)
                rate_user_star_3.setBackgroundResource(R.drawable.ic_star_full_2)
                rate_user_star_4.setBackgroundResource(R.drawable.ic_star_full_2)
                rate_user_star_5.setBackgroundResource(R.drawable.ic_empty_star)
            }
            3 -> {
                rate_user_star_1.setBackgroundResource(R.drawable.ic_star_full_2)
                rate_user_star_2.setBackgroundResource(R.drawable.ic_star_full_2)
                rate_user_star_3.setBackgroundResource(R.drawable.ic_star_full_2)
                rate_user_star_4.setBackgroundResource(R.drawable.ic_empty_star)
                rate_user_star_5.setBackgroundResource(R.drawable.ic_empty_star)
            }
            2 -> {
                rate_user_star_1.setBackgroundResource(R.drawable.ic_star_full_2)
                rate_user_star_2.setBackgroundResource(R.drawable.ic_star_full_2)
                rate_user_star_3.setBackgroundResource(R.drawable.ic_empty_star)
                rate_user_star_4.setBackgroundResource(R.drawable.ic_empty_star)
                rate_user_star_5.setBackgroundResource(R.drawable.ic_empty_star)
            }
            1 -> {
                rate_user_star_1.setBackgroundResource(R.drawable.ic_star_full_2)
                rate_user_star_2.setBackgroundResource(R.drawable.ic_empty_star)
                rate_user_star_3.setBackgroundResource(R.drawable.ic_empty_star)
                rate_user_star_4.setBackgroundResource(R.drawable.ic_empty_star)
                rate_user_star_5.setBackgroundResource(R.drawable.ic_empty_star)
            }
        }
    }

    override fun setReviewData(message: String, stars: String) {
        rate_user_message.setText(message)
        showStars(stars.toInt())
    }

    override fun showUserError() {
        Toast.makeText(this, R.string.rate_user_error, Toast.LENGTH_SHORT).show()
    }

    override fun setEditData() {
        review_button.text = getString(R.string.review_edit_text)
        review_title.text = getString(R.string.review_edit_text)
        review_delete_button.visibility = View.VISIBLE
    }

    override fun askDelete() {
        val builder1: AlertDialog.Builder = AlertDialog.Builder(this)
        builder1.setMessage(getString(R.string.review_delete_sure))
        builder1.setCancelable(true)

        builder1.setPositiveButton(
            getString(R.string.yes),
            DialogInterface.OnClickListener { dialog, id ->
                dialog.cancel()
                presenter.deleteReview()
            })

        builder1.setNegativeButton(
            getString(R.string.no),
            DialogInterface.OnClickListener { dialog, id -> dialog.cancel() })

        val alert11: AlertDialog = builder1.create()
        alert11.show()
    }

    override fun finishRate() {
        finish()
    }

}