package com.upc.tfm.weoffer.rateuser

import java.io.Serializable

data class ReviewRequest (
    val receiver_id: String,
    val stars: Int,
    val message: String
)

data class ReviewResponse (
    val sender_name: String,
    val sender_surname: String,
    val sender_image: String,
    val stars: Int,
    val message: String,
    val date: String
): Serializable

data class CheckReview (
    val id: String,
    val stars: String,
    val message: String,
    val found: Boolean
)

data class MyReview(
    val stars: Float,
    val count: Int
)