package com.upc.tfm.weoffer.utils

object UtilUser {

    var name: String? = null
    var surname: String? = null
    var gender: String? = null
    var birthday: String? = null
    var email: String? = null
    var country: String? = null
    var token: String? = null
    var id: String? = null
    var image: String? = null

    fun setData(data: ArrayList<String>) {
        name = data[0]
        surname = data[1]
        gender = data[2]
        birthday = data[3]
        email = data[4]
        country = data[5]
        token = data[6]
        id = data[7]
        image = data[8]
    }

    fun delete() {
        name = null
        surname = null
        gender = null
        birthday = null
        email = null
        country = null
        token = null
        id = null
        image = null
    }

}
