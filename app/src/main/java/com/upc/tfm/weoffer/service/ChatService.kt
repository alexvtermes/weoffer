package com.upc.tfm.weoffer.service

import com.upc.tfm.weoffer.chat.ChatRequest
import com.upc.tfm.weoffer.chat.TimeRequest
import com.upc.tfm.weoffer.network.*
import com.upc.tfm.weoffer.utils.UtilService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ChatService(private val delegate: ChatService.OnChatCompleted) {

    interface OnChatCompleted {
        fun onChatCreatedCompleted(code: Int)
        fun onGetListChatsCompleted(response: Pair<Int, ListChatsResponse?>)
        fun onChatDeleteCompleted(code: Int)
    }

    private var callbackCreateChat: Call<Unit>? = null
    private var callbackListChats: Call<ListChatsResponse>? = null
    private var callbackDelete: Call<Unit>? = null

    fun createChat(request: ChatRequest, token: String) {
        val retrofit = UtilService.prepareRetrofit()
        val service = retrofit.create(ChatAPI::class.java)
        UtilService.setPolicy()
        val header: MutableMap<String, String> = HashMap()
        header["Authorization"] = token
        callbackCreateChat = service.createChat(header, request)
        callbackCreateChat?.enqueue(object: Callback<Unit> {
            override fun onFailure(call: Call<Unit>?, t: Throwable) {
                if (call != null && call.isCanceled) {
                } else if (t is Exception) {
                    callbackCreateChat = null
                    delegate.onChatCreatedCompleted(900)
                }
                callbackCreateChat = null
            }

            override fun onResponse(call: Call<Unit>, response: Response<Unit>?) {
                if (response != null)
                    delegate.onChatCreatedCompleted(response.code())
                callbackCreateChat = null

            }
        })
    }

    fun getListChats(token: String) {
        val retrofit = UtilService.prepareRetrofit()
        val service = retrofit.create(ChatAPI::class.java)
        UtilService.setPolicy()
        val header: MutableMap<String, String> = HashMap()
        header["Authorization"] = token
        callbackListChats = service.getListChats(header)
        callbackListChats?.enqueue(object: Callback<ListChatsResponse>
        {
            override fun onFailure(call: Call<ListChatsResponse>?, t: Throwable) {
                if (call != null && call.isCanceled)
                {}
                else if (t is Exception)
                {
                    callbackListChats = null
                    delegate.onGetListChatsCompleted(Pair(900, null))
                }
                callbackListChats = null
            }

            override fun onResponse(call: Call<ListChatsResponse>, response: Response<ListChatsResponse>?) {
                if (response != null)
                    delegate.onGetListChatsCompleted(Pair(response.code(), response.body()))
                callbackListChats = null
            }
        })
    }

    fun updateTime(token: String, request: TimeRequest) {
        val retrofit = UtilService.prepareRetrofit()
        val service = retrofit.create(ChatAPI::class.java)
        UtilService.setPolicy()
        val header: MutableMap<String, String> = HashMap()
        header["Authorization"] = token
        val url = "/chat/update"
        callbackCreateChat = service.updateTime(url, header, request)
        callbackCreateChat?.enqueue(object: Callback<Unit>
        {
            override fun onFailure(call: Call<Unit>?, t: Throwable) {
                callbackCreateChat = null
            }

            override fun onResponse(call: Call<Unit>, response: Response<Unit>?) {
                callbackCreateChat = null
            }
        })
    }

    fun deleteChat(token: String, chatId: String) {
        val retrofit = UtilService.prepareRetrofit()
        val service = retrofit.create(ChatAPI::class.java)
        UtilService.setPolicy()
        val header: MutableMap<String, String> = HashMap()
        header["Authorization"] = token
        val url = "/chat/" + chatId
        callbackDelete = service.deleteChat(url, header)
        callbackDelete?.enqueue(object: Callback<Unit>
        {
            override fun onFailure(call: Call<Unit>?, t: Throwable) {
                if (call != null && call.isCanceled)
                {
                    //do nothing view does no exists anymore
                }
                else if (t is Exception)
                {
                    callbackDelete = null
                    delegate.onChatDeleteCompleted(900)
                }
                callbackDelete = null
            }

            override fun onResponse(call: Call<Unit>, response: Response<Unit>?) {
                if (response != null)
                    delegate.onChatDeleteCompleted(response.code())
                callbackDelete = null
            }
        })
    }

}