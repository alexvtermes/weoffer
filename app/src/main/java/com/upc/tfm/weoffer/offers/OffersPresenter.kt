package com.upc.tfm.weoffer.offers

import android.content.Context
import android.content.Intent
import com.upc.tfm.weoffer.network.ImagesResponse
import com.upc.tfm.weoffer.network.MyOffersResponse
import com.upc.tfm.weoffer.network.OfferDetailResponse
import com.upc.tfm.weoffer.network.OffersResponse
import com.upc.tfm.weoffer.offerdetail.OfferDetailView
import com.upc.tfm.weoffer.searchoffer.SearchOfferRequest
import com.upc.tfm.weoffer.searchoffer.SearchOfferView
import com.upc.tfm.weoffer.service.OfferService
import com.upc.tfm.weoffer.utils.UtilOffer
import com.upc.tfm.weoffer.utils.UtilUser

class OffersPresenter(val context: Context, val view: IOffersView): OfferService.OnOfferCompleted {

    private var offerService: OfferService = OfferService(this)
    private var listOffers: List<OfferRequest> = ArrayList()
    private var globalStep: Int = 0


    fun getOffers() {
        //if (UtilOffer.filterOffers != null) {
            //listOffers = UtilOffer.filterOffers!!.data!!
            if (UtilOffer.listOffers != null) {
                listOffers = UtilOffer.listOffers!!
                //view.setOffers(listOffers)
                //view.updateOffers(listOffers)
                //view.checkEmpty(listOffers)
            }
        //}
        else {
            val token = UtilUser.token
            if (token != null) {
                view.showProgressDialog()
                val request = SearchOfferRequest(
                    home = true,
                    step = 0,
                    query = null,
                    online = null,
                    priceRangeMax = null,
                    priceRangeMin = null,
                    order = null,
                    face = null
                )
                offerService.getOffers(token, request)
            }
        }
    }

    fun itemClicked(position: Int) {
        val offerId = listOffers[position].id
        val intent = Intent()
        intent.setClass(context, OfferDetailView::class.java)
        intent.putExtra("offer_id", offerId)
        context.startActivity(intent)
    }

    fun searchButtonPressed() {
        val intent = Intent()
        intent.setClass(context, SearchOfferView::class.java)
        context.startActivity(intent)
    }

    private fun handleResponse(code: Int, data: OffersResponse?) {
        when (code) {
            in 200..299 -> {
                if (data != null) {
                    if (data.data != null) {
                        listOffers += data.data
                        //UtilOffer.filterOffers = data
                        UtilOffer.listOffers = listOffers
                        view.updateOffers(data.data)
                        view.checkEmpty(listOffers)
                    }
                }
            }
        }
    }

    fun updateListOffers(offers: List<OfferRequest>) {
        listOffers = offers
    }

    fun bottomReached() {
            globalStep += 5
            val request = SearchOfferRequest(
                query = UtilOffer.query,
                online = UtilOffer.online,
                face = UtilOffer.face,
                priceRangeMin = UtilOffer.minPrice,
                priceRangeMax = UtilOffer.maxPrice,
                order = UtilOffer.order,
                step = globalStep,
                home = UtilOffer.home
            )
            if (UtilUser.token != null) {
                offerService.searchOffer(request, UtilUser.token!!)
            }
    }

    override fun onOffersCompleted(response: Pair<Int, OffersResponse?>) {
        view.closeDialog()
        handleResponse(response.first, response.second)
    }

    override fun onMyOffersCompleted(response: Pair<Int, MyOffersResponse?>) {}

    override fun onOfferCreatedCompleted(code: Int) {}

    override fun onOfferDetailCompleted(response: Pair<Int, OfferDetailResponse?>) {}

    override fun onDeleteOfferCompleted(code: Int) {}

    override fun onImagesCompleted(response: Pair<Int, ImagesResponse?>) {}

}