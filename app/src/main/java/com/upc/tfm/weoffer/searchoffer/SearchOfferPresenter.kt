package com.upc.tfm.weoffer.searchoffer

import android.content.Context
import android.content.Intent
import com.upc.tfm.weoffer.R
import com.upc.tfm.weoffer.home.HomeView
import com.upc.tfm.weoffer.network.ImagesResponse
import com.upc.tfm.weoffer.network.MyOffersResponse
import com.upc.tfm.weoffer.network.OfferDetailResponse
import com.upc.tfm.weoffer.network.OffersResponse
import com.upc.tfm.weoffer.service.OfferService
import com.upc.tfm.weoffer.utils.UtilOffer
import com.upc.tfm.weoffer.utils.UtilUser

class SearchOfferPresenter(val context: Context, val view: ISearchOfferView): OfferService.OnOfferCompleted {

    private var offerService: OfferService = OfferService(this)
    private var mQuery: String = ""
    private var mOnline: Boolean = false
    private var mMinPrice: Int? = null
    private var mMaxPrice: Int? = null
    private var mOrder: String? = null
    private var mFace: Boolean = false


    fun searchButtonPressed(query: String, online: Boolean?, face: Boolean?, priceRangeMin: Int, priceRangeMax: Int, order: String?) {
        val token = UtilUser.token
        if (token != null) {
            view.showProgressDialog()
            mQuery = query
            mOnline = online!!
            mFace = face!!
            if (!mOnline && !mFace) {
                view.showOnlineFaceError()
            }
            mMinPrice = priceRangeMin
            mMaxPrice = priceRangeMax
            mOrder = order
            val request = SearchOfferRequest(
                query = query,
                online = mOnline,
                face = mFace,
                priceRangeMin = priceRangeMin,
                priceRangeMax = priceRangeMax,
                order = order,
                step = 0,
                home = false
            )
            UtilOffer.listOffers = null
            offerService.searchOffer(request, token)
        }
    }

    fun resetFilters() {
        UtilOffer.delete()
        goToHomeView()
    }

    private fun goToHomeView() {

        val intent = Intent()
        intent.setClass(context, HomeView::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        context.startActivity(intent)


    }

    private fun handleResponse(code: Int, data: OffersResponse?) {
        when (code) {
            in 200..299 -> {
                if (data != null) {
                    UtilOffer.setFilters(data, mQuery, mOnline, mMinPrice, mMaxPrice, mOrder, mFace, false, data.data)
                    UtilOffer.listOffers = data.data
                    goToHomeView()
                }
            }
        }
    }

    fun getOrders(): Array<String> {
        return context.resources.getStringArray(R.array.search_orders)
    }

    // Override

    override fun onOffersCompleted(response: Pair<Int, OffersResponse?>) {
        view.closeDialog()
        handleResponse(response.first, response.second)
    }

    override fun onDeleteOfferCompleted(code: Int) {}

    override fun onMyOffersCompleted(response: Pair<Int, MyOffersResponse?>) {}

    override fun onOfferCreatedCompleted(code: Int) {}

    override fun onOfferDetailCompleted(response: Pair<Int, OfferDetailResponse?>) {}

    override fun onImagesCompleted(response: Pair<Int, ImagesResponse?>) {}

}