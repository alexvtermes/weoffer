package com.upc.tfm.weoffer.utils

import android.os.StrictMode
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object UtilService {

    const val baseUrl: String = "https://tfm-ruby.herokuapp.com"

    fun prepareRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(UtilService.baseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    fun setPolicy() {
        val policy = StrictMode.ThreadPolicy.Builder()
            .permitAll().build()
        StrictMode.setThreadPolicy(policy)
    }

}