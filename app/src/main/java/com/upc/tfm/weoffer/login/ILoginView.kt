package com.upc.tfm.weoffer.login

interface ILoginView {

    fun showEmptyEmail()
    fun showEmptyPassword()
    fun showEmailNotValid()
    fun showToast(text:String)
    fun showProgressDialog()
    fun closeDialog()

}