package com.upc.tfm.weoffer.utils

import com.upc.tfm.weoffer.network.OffersResponse
import com.upc.tfm.weoffer.offers.OfferRequest

object UtilOffer {

    var filterOffers: OffersResponse? = null
    var listOffers: List<OfferRequest>? = null
    var query: String? = ""
    var online: Boolean? = null
    var minPrice: Int? = null
    var maxPrice: Int? = null
    var order: String? = null
    var face: Boolean? = null
    var home: Boolean = true

    fun delete() {
        filterOffers = null
        listOffers = null
        query = ""
        online = null
        minPrice = null
        maxPrice = null
        order = null
        face = null
        home = true
    }

    fun setFilters(offers: OffersResponse?, filterQuery: String, filterOnline: Boolean?, min_price: Int?, max_price: Int?, order_by: String?, filterFace: Boolean?, filterHome: Boolean, list: List<OfferRequest>?) {
        filterOffers = offers
        listOffers = list
        query = filterQuery
        online = filterOnline
        minPrice = min_price
        maxPrice = max_price
        order = order_by
        face = filterFace
        home = filterHome
    }

}