package com.upc.tfm.weoffer.offerdetail

interface IOfferDetailView {

    fun showProgressDialog()
    fun closeDialog()
    fun showOfferDetail(data: OfferDetailRequest)
    fun askDelete()
    fun showImage(image: String)
    fun updateCount(count: Int, size: Int)

}