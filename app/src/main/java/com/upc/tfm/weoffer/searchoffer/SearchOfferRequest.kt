package com.upc.tfm.weoffer.searchoffer

data class SearchOfferRequest (
    val query: String?,
    val online: Boolean?,
    val priceRangeMin: Int?,
    val priceRangeMax: Int?,
    val order: String?,
    val step: Int,
    val face: Boolean?,
    val home: Boolean?
)