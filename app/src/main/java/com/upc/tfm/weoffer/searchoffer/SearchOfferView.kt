package com.upc.tfm.weoffer.searchoffer

import android.app.Dialog
import android.os.Bundle
import android.view.DragEvent
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.slider.LabelFormatter
import com.upc.tfm.weoffer.R
import com.upc.tfm.weoffer.utils.UtilOffer
import kotlinx.android.synthetic.main.register_view.*
import kotlinx.android.synthetic.main.search_offer_view.*
import java.text.NumberFormat
import java.util.*

class SearchOfferView: AppCompatActivity(), ISearchOfferView {

    private var presenter: SearchOfferPresenter = SearchOfferPresenter(this, this)
    private var dialog: Dialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.search_offer_view)
        init()
    }

    private fun init() {
        initSearchOfferButton()
        initRangeSlider()
        initSwitch()
        initSpinnerOrder()
        initData()
    }

    private fun initSearchOfferButton() {
        search_offer_button.setOnClickListener {
            presenter.searchButtonPressed(
                query = search_offer_key.text.toString(),
                online = search_offer_switch.isChecked,
                face = search_offer_switch_face.isChecked,
                priceRangeMin = range_slider.values[0].toInt(),
                priceRangeMax = range_slider.values[1].toInt(),
                order = search_order_by.selectedItem.toString()
            )
        }
        search_offer_reset.setOnClickListener {
            presenter.resetFilters()
        }
    }

    private fun initRangeSlider() {
        range_slider.setLabelFormatter(object: LabelFormatter {
            override fun getFormattedValue(value: Float): String {
                val currencyFormat = NumberFormat.getCurrencyInstance()
                currencyFormat.currency = Currency.getInstance("EUR")
                val price = value.toInt()
                price_range_min.text = range_slider.values[0].toInt().toString() + "€"
                price_range_max.text = range_slider.values[1].toInt().toString() + "€"
                if (range_slider.values[0] >= 100) {
                    price_range_min.text = "+ " + range_slider.values[0].toInt().toString() + "€"
                }
                if (range_slider.values[1] >= 100) {
                    price_range_max.text = "+ " + range_slider.values[1].toInt().toString() + "€"
                }
                return currencyFormat.format(price)
            }
        })
    }

    private fun initData() {
        if (UtilOffer.query != null)
            search_offer_key.setText(UtilOffer.query)
        if (UtilOffer.online != null) {
            search_offer_switch.isChecked = UtilOffer.online!!
        }
        if (UtilOffer.face != null) {
            search_offer_switch_face.isChecked = UtilOffer.face!!
        }
        if (UtilOffer.minPrice != null) {
            range_slider.values[0] = UtilOffer.minPrice!!.toFloat()
            price_range_min.text = range_slider.values[0].toInt().toString() + "€"
        }
        else {
            price_range_min.text = "0€"
        }
        if (UtilOffer.maxPrice != null) {
            range_slider.values[1] = UtilOffer.maxPrice!!.toFloat()
            price_range_max.text = range_slider.values[1].toInt().toString() + "€"
        }
        else {
            price_range_max.text = "+ 100€"
        }
        if (UtilOffer.order != null) {

        }
    }

    private fun initSwitch() {
        search_offer_switch.isChecked = true
        search_offer_switch_face.isChecked = true
    }

    private fun initSpinnerOrder() {
        var adapter = ArrayAdapter<String>(
            this,
            R.layout.spinner,
            presenter.getOrders())
        adapter.setDropDownViewResource(R.layout.spinner)
        search_order_by.adapter = adapter

        search_order_by.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                //presenter.genderTypeSelected(position)
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                //Nothing
            }
        }
    }

    // Override

    override fun showProgressDialog() {
        dialog = Dialog(this)
        dialog?.setContentView(R.layout.progressbar_layout)
        dialog?.setCancelable(false)
        dialog?.setCanceledOnTouchOutside(false)
        dialog?.show()
    }

    override fun closeDialog() {
        dialog?.dismiss()
    }

    override fun showOnlineFaceError() {
        Toast.makeText(this, getString(R.string.online_face_error), Toast.LENGTH_SHORT).show()
    }

}