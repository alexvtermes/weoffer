package com.upc.tfm.weoffer.createoffer

import com.upc.tfm.weoffer.offerdetail.ImageRequest

interface ICreateOfferView {

    fun showMissingFields()
    fun showMissingTitle()
    fun showMissingDescription()
    fun showMissingLocation()
    fun showMissingHourPrice()
    fun showMissingTotalPrice()
    fun showProgressDialog()
    fun closeDialog()
    fun showServerError()
    fun showImages(images: List<ImageRequest>?)
    fun finishCreate()

}