package com.upc.tfm.weoffer.myoffers

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.upc.tfm.weoffer.R
import kotlinx.android.synthetic.main.my_offers_item.view.*

class MyOffersAdapter(val context: Context, var myOffers: List<MyOfferRequest>, val presenter: MyOffersPresenter) : RecyclerView.Adapter<ViewHolder>() {

    fun getImage(image: String?): Bitmap? {
        val encodedString = image
        if (encodedString != null) {
            val encoded = encodedString.split(",")
            val decodedString = Base64.decode(encoded[1], Base64.DEFAULT)
            return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
        }
        return null
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.title?.text = myOffers[position].title
        holder.price?.text = (myOffers[position].hour_price.toFloat() + myOffers[position].total_price.toFloat()).toString()
        if (myOffers[position].image == null) {
            holder.image.setBackgroundResource(R.drawable.no_image)
        }
        else {
            val image = getImage(myOffers[position].image)
            if (image != null) {
                holder.image?.setImageDrawable(BitmapDrawable(context.resources, image))
                holder.image?.setImageBitmap(image)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.my_offers_item, parent, false))
    }

    override fun getItemCount(): Int {
        return myOffers.size
    }

    fun updateData(data: List<MyOfferRequest>) {
        myOffers = data
    }

}

class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {
    val title = view.my_offer_title
    val price = view.my_offer_price
    val image = view.my_offer_image
}

interface OnItemClickListener {
    fun onItemClicked(position:Int, view: View)
}


fun RecyclerView.addOnItemClickListener(onClickListener: OnItemClickListener) {
    this.addOnChildAttachStateChangeListener(object: RecyclerView.OnChildAttachStateChangeListener {
        override fun onChildViewDetachedFromWindow(view: View) {
            view.setOnClickListener(null)
        }

        override fun onChildViewAttachedToWindow(view: View) {
            view.setOnClickListener({
                val holder = getChildViewHolder(view)
                onClickListener.onItemClicked(holder.adapterPosition, view)
            })
        }
    })
}