package com.upc.tfm.weoffer.chat

data class ChatRequest (
    val user1_id: String,
    val user2_id: String,
    val offer_id: String
)


class Chat (
    var sender: String = "",
    var receiver: String = "",
    var message: String = "",
    var time: String = ""
)

data class TimeRequest (
    val sender: String,
    val receiver: String,
    val date: String,
    val time: String
)