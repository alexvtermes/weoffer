package com.upc.tfm.weoffer.network

import com.upc.tfm.weoffer.createoffer.CreateOfferRequest
import com.upc.tfm.weoffer.myoffers.MyOfferRequest
import com.upc.tfm.weoffer.offerdetail.ImageRequest
import com.upc.tfm.weoffer.offerdetail.OfferDetailRequest
import com.upc.tfm.weoffer.offers.OfferRequest
import com.upc.tfm.weoffer.searchoffer.SearchOfferRequest
import retrofit2.Call
import retrofit2.http.*
import java.io.Serializable

data class MyOffersResponse constructor(val status: String, val message: String, val data: List<MyOfferRequest>): Serializable

data class OfferDetailResponse constructor(val status: String, val message: String, val data: OfferDetailRequest)

data class OffersResponse constructor(val status: String, val message: String, val data: List<OfferRequest>?)

data class ImagesResponse constructor(val status: String, val message: String, val data: List<ImageRequest>?)


interface OfferAPI {

    @POST("/offer/create")
    fun createOffer(@HeaderMap header: Map<String, String>, @Body createOfferRequest: CreateOfferRequest): Call<Unit>

    @GET("/offer/my")
    fun getMyOffers(@HeaderMap header: Map<String, String>): Call<MyOffersResponse>

    @GET()
    fun getOfferDetail(@Url url: String, @HeaderMap header: Map<String, String>): Call<OfferDetailResponse>
/*
    @GET("/offers/home")
    fun getOffers(@HeaderMap header: Map<String, String>): Call<OffersResponse>
*/
    @GET()
    fun getOtherOffers(@Url url: String, @HeaderMap header: Map<String, String>): Call<MyOffersResponse>

    @DELETE()
    fun deleteOffer(@Url url: String, @HeaderMap header: Map<String, String>): Call<Unit>

    @PUT()
    fun editOffer(@Url url: String, @HeaderMap header: Map<String, String>, @Body createOfferRequest: CreateOfferRequest): Call<Unit>

    @PUT("/search")
    fun searchOffer(@HeaderMap header: Map<String, String>, @Body searchOfferRequest: SearchOfferRequest): Call<OffersResponse>

    @GET()
    fun getImages(@Url url: String, @HeaderMap header: Map<String, String>): Call<ImagesResponse>

}