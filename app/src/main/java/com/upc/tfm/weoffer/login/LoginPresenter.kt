package com.upc.tfm.weoffer.login

import android.content.Context
import android.content.Intent
import androidx.core.content.ContextCompat.startActivity
import com.upc.tfm.weoffer.R
import com.upc.tfm.weoffer.home.HomeView
import com.upc.tfm.weoffer.register.RegisterPresenter
import com.upc.tfm.weoffer.register.RegisterView
import com.upc.tfm.weoffer.service.UserService
import com.upc.tfm.weoffer.storage.InternalStorage
import java.util.regex.Matcher
import java.util.regex.Pattern

class LoginPresenter(val context: Context, val view: ILoginView): UserService.OnUserCompleted {

    private var userService: UserService
    private val internalStorage: InternalStorage

    private var myEmail: String = ""
    private var myPassword: String = ""

    init {
        userService = UserService(this)
        internalStorage = InternalStorage()
    }

    fun loginButtonPressed(email: String, password: String) {
        if (checkRequiredFields(email, password)) {
            if (isEmailValid(email)) {
                doLogin(email, password)
            }
            else view.showEmailNotValid()
        }
    }

    private fun doLogin(email: String, password: String) {
        view.showProgressDialog()
        myEmail = email
        myPassword = password
        val request = LoginRequest(
            email = email,
            password = password
        )
        userService.doLogin(request)
    }

    fun registerButtonPressed() {
        val intent = Intent()
        intent.setClass(context, RegisterView::class.java)
        context.startActivity(intent)
    }

    private fun checkRequiredFields(email: String, password: String): Boolean {
        var correct = true
        if (email.isEmpty()) {
            view.showEmptyEmail()
            correct = false
        }
        if (password.isEmpty()) {
            view.showEmptyPassword()
            correct = false
        }
        return correct
    }

    private fun isEmailValid(email: String?): Boolean {
        val expression: String = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$"
        val pattern: Pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE)
        val matcher: Matcher = pattern.matcher(email)
        return matcher.matches()
    }

    private fun handleResponse(code: Int, response: LoginResponse?): Boolean {
        when (code) {
            in 200..299 -> {
                if (response != null) {
                    internalStorage.storeUser(context, response)
                    view.showToast(context.getString(R.string.login_correct))
                    goToHomeView()
                }
                return true
            }
            else -> {
                view.showToast(context.getString(R.string.login_error))
            }
        }
        return false
    }

    private fun goToHomeView() {
        val intent = Intent()
        intent.setClass(context, HomeView::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        context.startActivity(intent)
    }

    //Override

    override fun onLoginCompleted(response: Pair<Int, LoginResponse?>) {
        handleResponse(response.first, response.second)
        view.closeDialog()
    }

    override fun onRegisterCompleted(code: Int) {}

    override fun onImageUpdated(code: Int) {}

    override fun onOtherUserInfoCompleted(response: Pair<Int, LoginResponse?>) {}

}