package com.upc.tfm.weoffer.rateuser

interface IRateUserView {

    fun showStars(stars: Int)
    fun showStarsError()
    fun showProgressDialog()
    fun closeDialog()
    fun showReviewCorrectly()
    fun showUserError()
    fun setReviewData(message: String, stars: String)
    fun setEditData()
    fun askDelete()
    fun finishRate()

}