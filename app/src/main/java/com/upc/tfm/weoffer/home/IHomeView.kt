package com.upc.tfm.weoffer.home

interface IHomeView {

    fun closeDrawable()
    fun setTitle(title: Int)

}