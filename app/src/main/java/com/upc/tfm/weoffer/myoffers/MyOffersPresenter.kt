package com.upc.tfm.weoffer.myoffers

import android.content.Context
import android.content.Intent
import com.upc.tfm.weoffer.createoffer.CreateOfferView
import com.upc.tfm.weoffer.network.ImagesResponse
import com.upc.tfm.weoffer.network.MyOffersResponse
import com.upc.tfm.weoffer.network.OfferDetailResponse
import com.upc.tfm.weoffer.network.OffersResponse
import com.upc.tfm.weoffer.offerdetail.OfferDetailView
import com.upc.tfm.weoffer.service.OfferService
import com.upc.tfm.weoffer.utils.UtilUser

class MyOffersPresenter(val context: Context, val view: IMyOffersView): OfferService.OnOfferCompleted {

    private var offerService: OfferService = OfferService(this)
    private var myOffers: List<MyOfferRequest> = ArrayList()


    fun addButtonPressed() {
        val intent = Intent()
        intent.setClass(context, CreateOfferView::class.java)
        intent.putExtra("edit", "0")
        context.startActivity(intent)
    }

    fun itemClicked(position: Int) {
        val offerId = myOffers[position].id
        val intent = Intent()
        intent.setClass(context, OfferDetailView::class.java)
        intent.putExtra("offer_id", offerId)
        context.startActivity(intent)
    }

    fun getMyOffers() {
        val token = UtilUser.token
        if (token != null) {
            view.showProgressDialog()
            offerService.getMyOffers(token)
        }
    }

    private fun handleResponse(code: Int, data: MyOffersResponse?) {
        when (code) {
            in 200..299 -> {
                if (data != null) {
                    view.setMyOffers(data.data)
                    myOffers = data.data
                }
            }
        }
    }

    // Override

    override fun onMyOffersCompleted(response: Pair<Int, MyOffersResponse?>) {
        view.closeDialog()
        handleResponse(response.first, response.second)
    }

    override fun onOfferCreatedCompleted(code: Int) {}
    override fun onOfferDetailCompleted(response: Pair<Int, OfferDetailResponse?>) {}
    override fun onOffersCompleted(response: Pair<Int, OffersResponse?>) {}
    override fun onDeleteOfferCompleted(code: Int) {}
    override fun onImagesCompleted(response: Pair<Int, ImagesResponse?>) {}

}