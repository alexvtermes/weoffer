package com.upc.tfm.weoffer.chat

interface IChatView {

    fun showProgressDialog()
    fun closeDialog()
    fun askDelete()

}