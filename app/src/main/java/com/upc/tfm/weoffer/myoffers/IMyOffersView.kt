package com.upc.tfm.weoffer.myoffers

interface IMyOffersView {

    fun showToast(msg: String)
    fun showProgressDialog()
    fun closeDialog()
    fun setMyOffers(data: List<MyOfferRequest>)
    fun reloadRecyclerView(data: List<MyOfferRequest>)

}