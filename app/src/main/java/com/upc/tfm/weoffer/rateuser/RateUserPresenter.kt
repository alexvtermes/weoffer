package com.upc.tfm.weoffer.rateuser

import android.content.Context
import android.content.Intent
import com.upc.tfm.weoffer.home.HomeView
import com.upc.tfm.weoffer.network.CheckReviewResponse
import com.upc.tfm.weoffer.network.MyReviewResponse
import com.upc.tfm.weoffer.network.ReviewsResponse
import com.upc.tfm.weoffer.service.ReviewService
import com.upc.tfm.weoffer.utils.UtilUser

class RateUserPresenter(val context: Context, val view: IRateUserView): ReviewService.OnReviewCompleted {

    private var service: ReviewService = ReviewService(this)
    private var stars = 0
    private var mUserId = "0"

    fun rateButtonPressed(message: String) {
        if (stars != 0) {
            createReview(message)
        }
        else
            view.showStarsError()
    }

    private fun createReview(message: String) {
        val token = UtilUser.token
        if (token != null) {
            val request = ReviewRequest(
                message = message,
                stars = stars,
                receiver_id = mUserId
            )
            view.showProgressDialog()
            service.createReview(request, token)
        }
    }

    fun checkReview() {
        val token = UtilUser.token
        if (token != null) {
            view.showProgressDialog()
            service.checkReview(token, mUserId)
        }
    }

    fun setUserId(userId: String?) {
        if (userId != null)
            mUserId = userId
        else
            view.showUserError()
    }

    fun star1Pressed() {
        stars = 1
        view.showStars(1)
    }

    fun star2Pressed() {
        stars = 2
        view.showStars(2)
    }

    fun star3Pressed() {
        stars = 3
        view.showStars(3)
    }

    fun star4Pressed() {
        stars = 4
        view.showStars(4)
    }

    fun star5Pressed() {
        stars = 5
        view.showStars(5)
    }

    private fun goToProfileView() {
        /*val intent = Intent()
        intent.setClass(context, HomeView::class.java)
        intent.putExtra("view", "1")
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        context.startActivity(intent)*/
        view.finishRate()
    }

    private fun handleReviewCompleted(code: Int) {
        when (code) {
            in 200..299 -> {
                goToProfileView()
            }
        }
    }

    private fun handleCheckReview(code: Int, data: CheckReviewResponse?) {
        when (code) {
            in 200..299 -> {
                if (data != null) {
                    if (data.data.found) {
                        stars = data.data.stars.toInt()
                        view.setEditData()
                        view.setReviewData(data.data.message, data.data.stars)
                    }
                }
            }
        }
    }

    fun deleteButtonPressed() {
        view.askDelete()
    }

    fun deleteReview() {
        val token = UtilUser.token
        if (token != null) {
            view.showProgressDialog()
            service.deleteReview(token, mUserId)
        }
    }

    private fun handleDeleteReview(code: Int) {
        when (code) {
            in 200..299 -> {
                goToProfileView()
            }
        }
    }

    // Override

    override fun onReviewCreatedCompleted(code: Int) {
        view.closeDialog()
        handleReviewCompleted(code)
    }

    override fun onCheckReviewCompleted(response: Pair<Int, CheckReviewResponse?>) {
        view.closeDialog()
        handleCheckReview(response.first, response.second)
    }

    override fun onDeleteReviewCompleted(code: Int) {
        view.closeDialog()
        handleDeleteReview(code)
    }

    override fun onGetMyReviewCompleted(response: Pair<Int, MyReviewResponse?>) {}

    override fun onGetReviewsCompleted(response: Pair<Int, ReviewsResponse?>) {}

}
