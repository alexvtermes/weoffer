package com.upc.tfm.weoffer.network

import com.upc.tfm.weoffer.chat.ChatRequest
import com.upc.tfm.weoffer.chat.TimeRequest
import com.upc.tfm.weoffer.listchats.ListChatsRequest
import retrofit2.Call
import retrofit2.http.*

data class ListChatsResponse constructor(val status: String, val message: String, val data: List<ListChatsRequest>)

interface ChatAPI {

    @POST("/chat/create")
    fun createChat(@HeaderMap header: Map<String, String>, @Body chatRequest: ChatRequest): Call<Unit>

    @GET("/chat/my")
    fun getListChats(@HeaderMap header: Map<String, String>): Call<ListChatsResponse>

    @PUT()
    fun updateTime(@Url url: String, @HeaderMap header: Map<String, String>, @Body request: TimeRequest): Call<Unit>

    @DELETE()
    fun deleteChat(@Url url: String, @HeaderMap header: Map<String, String>): Call<Unit>

}