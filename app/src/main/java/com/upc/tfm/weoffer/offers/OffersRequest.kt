package com.upc.tfm.weoffer.offers

data class OfferRequest (
    val id: String,
    val title: String,
    val hour_price: String,
    val total_price: String,
    val image: String?,
    val user_image: String?,
    val user_name: String,
    val user_stars: Float,
    val user_count: Int,
    val online: Boolean,
    val date: String
)