package com.upc.tfm.weoffer.profile

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.os.Bundle
import android.util.Base64
import android.view.*
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.upc.tfm.weoffer.R
import com.upc.tfm.weoffer.login.LoginResponse
import com.upc.tfm.weoffer.network.ReviewsResponse
import com.upc.tfm.weoffer.profile.offers.ProfileOffersFragment
import com.upc.tfm.weoffer.profile.reviews.ProfileReviewsFragment
import com.upc.tfm.weoffer.utils.UtilUser
import kotlinx.android.synthetic.main.profile_review_fragment.*
import kotlinx.android.synthetic.main.profile_view.*
import kotlinx.android.synthetic.main.register_view.*
import java.io.InputStream

class ProfileView: Fragment(), IProfileView {

    private lateinit var presenter: ProfilePresenter
    private lateinit var activity: Activity
    private var dialog: Dialog? = null
    private lateinit var fragment_reviews: ProfileReviewsFragment
    private lateinit var fragment_offers: ProfileOffersFragment
    private var reviewsBundle: Bundle = Bundle()
    private var offersBundle: Bundle = Bundle()
    private var other: Boolean = false
    private var first: Boolean = true


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.profile_view, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)
        (activity as AppCompatActivity).supportActionBar?.title = activity.getString(R.string.profile_title)
        init()
    }

    override fun onResume() {
        super.onResume()
        if (!first) {
            initData()
            initTabView()
        }
        first = false
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu?.clear()
        inflater?.inflate(R.menu.main_drawer, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    private fun init() {
        presenter = ProfilePresenter(activity, this)
        other = checkPrivacy()
        initData()
        initTabView()
        initFloatingButton()
    }

    private fun initFloatingButton() {
        if (other) {
            profile_fab.setOnClickListener {
                presenter.reviewButtonPressed()
            }
        }
        else
            profile_fab.hide()
    }

    private fun checkPrivacy(): Boolean {
        if (arguments != null) {
            if (arguments!!.get("other") == "1")
                return true
        }
        return false
    }

    private fun getMonth(monthNumber: String): String {
        return when (monthNumber.toInt()) {
            1 -> getString(R.string.January)
            2 -> getString(R.string.February)
            3 -> getString(R.string.March)
            4 -> getString(R.string.April)
            5 -> getString(R.string.May)
            6 -> getString(R.string.June)
            7 -> getString(R.string.July)
            8 -> getString(R.string.August)
            9 -> getString(R.string.September)
            10 -> getString(R.string.October)
            11 -> getString(R.string.November)
            12 -> getString(R.string.December)
            else -> ""
        }
    }

    private fun getBirthDate(date: String?): String {
        if (date != null) {
            val separated = date.split("/")
            val day = separated[0]
            val month = getMonth(separated[1])
            val year = separated[2]
            return day + " " + month + " " + year
        }
        return ""
    }

    private fun initData() {
        if (!other) {
            val userData = presenter.getInfoUser()
            profile_name.text = userData[0] + " " + userData[1]
            profile_birthday.text = getBirthDate(userData[4])
            profile_country.text = userData[3]
            profile_email.text = userData[2]
            if (userData[5] != null)
                profile_image.setImageBitmap(getBitmap(userData[5]!!))
            profile_image.setOnClickListener {
                presenter.imageClicked()
            }
            presenter.getReviewData()
        }
        else {
            val userId = getUserId()
            if (userId != null) {
                if (userId == UtilUser.id) {
                    profile_fab.hide()
                }
                presenter.getInfoOtherUser(userId)
            }
        }
    }

    private fun getUserId(): String? {
        if (arguments != null)
            return arguments!!.get("user_id").toString()
        return null
    }

    private fun getBitmap(image: String): Bitmap? {
        val encoded = image.split(",")
        val decodedString = Base64.decode(encoded[1], Base64.DEFAULT)
        return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
    }

    private fun initTabView() {
        presenter.getReviews(getUserId())
    }

    private fun showStars(stars: Float) {
        when (stars) {
            in 4.5..5.0 -> {
                profile_star_1.setBackgroundResource(R.drawable.ic_star_full_2)
                profile_star_2.setBackgroundResource(R.drawable.ic_star_full_2)
                profile_star_3.setBackgroundResource(R.drawable.ic_star_full_2)
                profile_star_4.setBackgroundResource(R.drawable.ic_star_full_2)
                profile_star_5.setBackgroundResource(R.drawable.ic_star_full_2)
            }
            in 3.5..4.5 -> {
                profile_star_1.setBackgroundResource(R.drawable.ic_star_full_2)
                profile_star_2.setBackgroundResource(R.drawable.ic_star_full_2)
                profile_star_3.setBackgroundResource(R.drawable.ic_star_full_2)
                profile_star_4.setBackgroundResource(R.drawable.ic_star_full_2)
                profile_star_5.setBackgroundResource(R.drawable.ic_empty_star)
            }
            in 2.5..3.5 -> {
                profile_star_1.setBackgroundResource(R.drawable.ic_star_full_2)
                profile_star_2.setBackgroundResource(R.drawable.ic_star_full_2)
                profile_star_3.setBackgroundResource(R.drawable.ic_star_full_2)
                profile_star_4.setBackgroundResource(R.drawable.ic_empty_star)
                profile_star_5.setBackgroundResource(R.drawable.ic_empty_star)
            }
            in 1.5..2.5 -> {
                profile_star_1.setBackgroundResource(R.drawable.ic_star_full_2)
                profile_star_2.setBackgroundResource(R.drawable.ic_star_full_2)
                profile_star_3.setBackgroundResource(R.drawable.ic_empty_star)
                profile_star_4.setBackgroundResource(R.drawable.ic_empty_star)
                profile_star_5.setBackgroundResource(R.drawable.ic_empty_star)
            }
            in 0.5..1.5 -> {
                profile_star_1.setBackgroundResource(R.drawable.ic_star_full_2)
                profile_star_2.setBackgroundResource(R.drawable.ic_empty_star)
                profile_star_3.setBackgroundResource(R.drawable.ic_empty_star)
                profile_star_4.setBackgroundResource(R.drawable.ic_empty_star)
                profile_star_5.setBackgroundResource(R.drawable.ic_empty_star)
            }
            else -> {
                profile_star_1.setBackgroundResource(R.drawable.ic_empty_star)
                profile_star_2.setBackgroundResource(R.drawable.ic_empty_star)
                profile_star_3.setBackgroundResource(R.drawable.ic_empty_star)
                profile_star_4.setBackgroundResource(R.drawable.ic_empty_star)
                profile_star_5.setBackgroundResource(R.drawable.ic_empty_star)
            }
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        activity = context as (Activity)
    }

    override fun showProgressDialog() {
        dialog = Dialog(activity)
        dialog?.setContentView(R.layout.progressbar_layout)
        dialog?.setCancelable(false)
        dialog?.setCanceledOnTouchOutside(false)
        dialog?.show()
    }

    override fun closeDialog() {
        dialog?.dismiss()
    }

    override fun setOffersData(data: Bundle) {
        offersBundle = data
        fragment_reviews = ProfileReviewsFragment()
        fragment_offers = ProfileOffersFragment()
        val bundle = Bundle()
        fragment_reviews.arguments = bundle
        fragment_offers.arguments = bundle
        val adapter = ProfilePagerAdapter(((context as FragmentActivity).supportFragmentManager), reviewsBundle, offersBundle, fragment_reviews, fragment_offers)
        profile_view_pager.adapter = adapter
        profile_tabs.setupWithViewPager(profile_view_pager, true)
        profile_tabs.setSelectedTabIndicatorColor(Color.parseColor("#b3d9ff"))
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            try {
                val imageUri = data?.data
                if (imageUri != null) {
                    val imageStream: InputStream? = activity.contentResolver.openInputStream(imageUri)
                    val imageBitmap: Bitmap = BitmapFactory.decodeStream(imageStream)
                    profile_image.setImageBitmap(imageBitmap)
                    presenter.updateProfileImage(imageBitmap)
                }
            } catch (exception: Exception) {
                Toast.makeText(activity, "Something went wrong", Toast.LENGTH_LONG).show();
            }
        }
    }

    override fun showMessage(msg: String) {
        Toast.makeText(activity, msg, Toast.LENGTH_LONG).show();
    }

    override fun setOtherUserData(data: LoginResponse) {
        profile_name.text = data.name + " " + data.surname
        profile_birthday.text = getBirthDate(data.birthday)
        profile_country.text = data.country
        profile_email.text = data.email
        if (data.image != null)
            profile_image.setImageBitmap(getBitmap(data.image!!))
        profile_review_mark.text = data.stars.toString()
        profile_review_count.text = data.count.toString()
        if (data.stars != null)
            showStars(data.stars)
    }

    override fun setReviewsData(data: Bundle) {
        reviewsBundle = data

        presenter.getOffers(other)
    }

    override fun setReviewCountData(count: Int, stars: Float) {
        profile_review_count.text = count.toString()
        profile_review_mark.text = stars.toString()
        showStars(stars)
    }

    override fun onDestroy() {
        val transactionReviews = fragmentManager?.beginTransaction()
        transactionReviews?.remove(fragment_reviews)?.commitAllowingStateLoss()
        val transactionOffers = fragmentManager?.beginTransaction()
        transactionOffers?.remove(fragment_offers)?.commitAllowingStateLoss()
        super.onDestroy()
    }

}