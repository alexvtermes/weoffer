package com.upc.tfm.weoffer.network

import com.upc.tfm.weoffer.login.LoginRequest
import com.upc.tfm.weoffer.login.LoginResponse
import com.upc.tfm.weoffer.login.ProfileImageRequest
import com.upc.tfm.weoffer.register.RegisterRequest
import retrofit2.Call
import retrofit2.http.*

data class UserResponse constructor(val status: String, val message: String, val data: LoginResponse)

interface UserAPI {

    @POST("/user/create")
    fun register(@Body registerRequest: RegisterRequest): Call<Unit>

    @POST("/user/login")
    fun login(@Body loginRequest: LoginRequest) : Call<UserResponse>

    @POST("/user/image/update")
    fun updateImage(@HeaderMap header: Map<String, String>, @Body imageRequest: ProfileImageRequest): Call<Unit>

    @GET()
    fun getOtherUserInfo(@Url url: String, @HeaderMap header: Map<String, String>): Call<UserResponse>

}

