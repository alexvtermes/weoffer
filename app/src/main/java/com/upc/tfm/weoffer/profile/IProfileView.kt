package com.upc.tfm.weoffer.profile

import android.os.Bundle
import com.upc.tfm.weoffer.login.LoginResponse
import com.upc.tfm.weoffer.network.ReviewsResponse

interface IProfileView {

    fun showProgressDialog()
    fun closeDialog()
    fun setOffersData(data: Bundle)
    fun showMessage(msg: String)
    fun setOtherUserData(data: LoginResponse)
    fun setReviewsData(data: Bundle)
    fun setReviewCountData(count: Int, stars: Float)

}