package com.upc.tfm.weoffer.network
import com.upc.tfm.weoffer.rateuser.CheckReview
import com.upc.tfm.weoffer.rateuser.MyReview
import com.upc.tfm.weoffer.rateuser.ReviewRequest
import com.upc.tfm.weoffer.rateuser.ReviewResponse
import retrofit2.Call
import retrofit2.http.*
import java.io.Serializable

data class ReviewsResponse constructor(val status: String, val message: String, val data: List<ReviewResponse>): Serializable

data class CheckReviewResponse constructor(val status: String, val message: String, val data: CheckReview)

data class MyReviewResponse constructor(val status: String, val message: String, val data: MyReview): Serializable

interface ReviewAPI {

    @POST("/review/create")
    fun createReview(@HeaderMap header: Map<String, String>, @Body reviewRequest: ReviewRequest): Call<Unit>

    @GET()
    fun getReviews(@Url url: String, @HeaderMap header: Map<String, String>): Call<ReviewsResponse>

    @GET()
    fun checkReview(@Url url: String, @HeaderMap header: Map<String, String>): Call<CheckReviewResponse>

    @DELETE()
    fun deleteReview(@Url url: String, @HeaderMap header: Map<String, String>): Call<Unit>

    @GET("/data/review")
    fun getMyReviews(@HeaderMap header: Map<String, String>): Call<MyReviewResponse>

}