package com.upc.tfm.weoffer.offerdetail

data class OfferDetailRequest (
    val id: String,
    val title: String,
    val description: String,
    val image: String?,
    val online: Boolean,
    val hour_price: Float,
    val total_price: Float,
    val location: String,
    val id_user: String,
    val name_user: String,
    val image_user: String,
    val stars: Float,
    val count: Int,
    val date: String,
    val images: ArrayList<ImageRequest>
)

data class ImageRequest (
    val id: String?,
    val image: String?,
    val offer_id: String?
)