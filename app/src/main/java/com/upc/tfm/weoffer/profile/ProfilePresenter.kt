package com.upc.tfm.weoffer.profile

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import android.util.Base64
import androidx.core.app.ActivityCompat
import com.upc.tfm.weoffer.R
import com.upc.tfm.weoffer.home.HomeView
import com.upc.tfm.weoffer.login.LoginResponse
import com.upc.tfm.weoffer.login.ProfileImageRequest
import com.upc.tfm.weoffer.network.*
import com.upc.tfm.weoffer.offerdetail.OfferDetailView
import com.upc.tfm.weoffer.rateuser.RateUserView
import com.upc.tfm.weoffer.service.OfferService
import com.upc.tfm.weoffer.service.ReviewService
import com.upc.tfm.weoffer.service.UserService
import com.upc.tfm.weoffer.storage.InternalStorage
import com.upc.tfm.weoffer.utils.UtilUser
import kotlinx.android.synthetic.main.home_view.*
import kotlinx.android.synthetic.main.nav_header_main.view.*
import java.io.ByteArrayOutputStream

class ProfilePresenter(val context: Context, val view: IProfileView): OfferService.OnOfferCompleted, UserService.OnUserCompleted, ReviewService.OnReviewCompleted {

    private var offerService: OfferService = OfferService(this)
    private var userService: UserService = UserService(this)
    private var reviewService: ReviewService = ReviewService(this)
    private var internalStorage: InternalStorage = InternalStorage()
    private var otherUserId: String = ""
    private var myImage: String = ""


    fun getInfoUser(): MutableList<String?> {
        val userData = mutableListOf<String?>()
        userData.add(UtilUser.name)
        userData.add(UtilUser.surname)
        userData.add(UtilUser.email)
        userData.add(UtilUser.country)
        userData.add(UtilUser.birthday)
        userData.add(UtilUser.image)
        return userData
    }

    fun getInfoOtherUser(userId: String) {
        //view.showProgressDialog()
        otherUserId = userId
        val token = UtilUser.token
        if (token != null) {
            userService.getOtherUserInfo(token, userId)
        }
    }

    fun getReviews(userId: String?) {
        //view.showProgressDialog()
        var id = userId
        val token = UtilUser.token
        if (userId == null) {
            id = UtilUser.id
        }
        if (token != null && id != null) {
            reviewService.getReviews(token, id)
        }
    }

    fun getOffers(other: Boolean) {
        val token = UtilUser.token
        if (token != null) {
            if (other)
                offerService.getOtherOffers(token, otherUserId)
            else
                offerService.getMyOffers(token)
        }
    }

    fun reviewButtonPressed() {
        val intent = Intent()
        intent.setClass(context, RateUserView::class.java)
        intent.putExtra("id", otherUserId)
        context.startActivity(intent)
    }

    private fun handleResponse(code: Int, response: MyOffersResponse?) {
        when (code) {
            in 200..299 -> {
                if (response != null) {
                    val data = Bundle()
                    data.putSerializable("Object", response)
                    view.setOffersData(data)
                }
            }
        }
    }

    fun imageClicked() {
        val galleryIntent = Intent(
            Intent.ACTION_PICK,
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        )
        ActivityCompat.startActivityForResult(
            context as Activity,
            galleryIntent,
            1,
            null
        )
    }

    fun updateProfileImage(image: Bitmap) {
        val token = UtilUser.token
        if (token != null) {
            view.showProgressDialog()
            var bos = ByteArrayOutputStream()
            image?.compress(Bitmap.CompressFormat.WEBP, 50, bos)
            var encodedImage = Base64.encodeToString(bos.toByteArray(), Base64.NO_WRAP)
            encodedImage = "data:image/png;base64," + encodedImage
            myImage = encodedImage
            val request = ProfileImageRequest(
                image = encodedImage
            )
            userService.updateProfileImage(request, token)
        }
    }

    private fun goToHomeView() {
        val intent = Intent()
        intent.setClass(context, HomeView::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        context.startActivity(intent)
    }

    private fun handleOtherUserInfoResponse(code: Int, data: LoginResponse?) {
        when (code) {
            in 200..299 -> {
                if (data != null) {
                    view.setOtherUserData(data)
                }
            }
        }
    }

    private fun handleReviewsResponse(code: Int, response: ReviewsResponse?) {
        when (code) {
            in 200..299 -> {
                if (response != null) {
                    val data = Bundle()
                    data.putSerializable("Object", response)
                    view.setReviewsData(data)
                }
            }
        }
    }

    fun getReviewData() {
        val token = UtilUser.token
        if (token != null) {
            view.showProgressDialog()
            reviewService.getMyReviews(token)
        }
    }

    private fun handleMyReview(code: Int, data: MyReviewResponse?) {
        when (code) {
            in 200..299 -> {
                if (data != null) {
                    view.setReviewCountData(data.data.count, data.data.stars)
                }
            }
        }
    }

    // Override

    override fun onMyOffersCompleted(response: Pair<Int, MyOffersResponse?>) {
        view.closeDialog()
        handleResponse(response.first, response.second)
    }

    override fun onImageUpdated(code: Int) {
        view.closeDialog()
        if (code in 200..299) {
            internalStorage.updateImage(context, myImage)
            view.showMessage(context.getString(R.string.profile_image_updated))
            goToHomeView()
        }
        else
            view.showMessage(context.getString(R.string.profile_error_image_updated))
    }


    override fun onGetReviewsCompleted(response: Pair<Int, ReviewsResponse?>) {
        handleReviewsResponse(response.first, response.second)
    }

    override fun onOtherUserInfoCompleted(response: Pair<Int, LoginResponse?>) {
        //view.closeDialog()
        handleOtherUserInfoResponse(response.first, response.second)
    }

    override fun onGetMyReviewCompleted(response: Pair<Int, MyReviewResponse?>) {
        view.closeDialog()
        handleMyReview(response.first, response.second)
    }

    override fun onReviewCreatedCompleted(code: Int) {}

    override fun onOfferCreatedCompleted(code: Int) {}

    override fun onOfferDetailCompleted(response: Pair<Int, OfferDetailResponse?>) {}

    override fun onLoginCompleted(response: Pair<Int, LoginResponse?>) {}

    override fun onRegisterCompleted(code: Int) {}

    override fun onOffersCompleted(response: Pair<Int, OffersResponse?>) {}

    override fun onDeleteOfferCompleted(code: Int) {}

    override fun onCheckReviewCompleted(response: Pair<Int, CheckReviewResponse?>) {}

    override fun onDeleteReviewCompleted(code: Int) {}

    override fun onImagesCompleted(response: Pair<Int, ImagesResponse?>) {}

}