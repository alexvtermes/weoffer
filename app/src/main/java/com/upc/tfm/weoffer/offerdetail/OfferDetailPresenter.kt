package com.upc.tfm.weoffer.offerdetail

import android.content.Context
import android.content.Intent
import com.upc.tfm.weoffer.buy.BuyView
import com.upc.tfm.weoffer.chat.ChatRequest
import com.upc.tfm.weoffer.chat.ChatView
import com.upc.tfm.weoffer.createoffer.CreateOfferView
import com.upc.tfm.weoffer.home.HomeView
import com.upc.tfm.weoffer.network.*
import com.upc.tfm.weoffer.profile.OtherView
import com.upc.tfm.weoffer.profile.ProfileView
import com.upc.tfm.weoffer.service.ChatService
import com.upc.tfm.weoffer.service.OfferService
import com.upc.tfm.weoffer.utils.UtilUser

class OfferDetailPresenter(val context: Context, val view: IOfferDetailView): OfferService.OnOfferCompleted, ChatService.OnChatCompleted {

    private val offerService: OfferService = OfferService(this)
    private val chatService: ChatService = ChatService(this)
    private var user_id: String? = null
    private var offer_id: String? = null
    private var user_name: String? = null
    private var user_image: String? = null
    private var offerInfo: OfferDetailRequest? = null
    private var array_images: ArrayList<ImageRequest> = ArrayList()
    private var index: Int = 0

    fun getOfferDetail(offerId: String?) {
        val token = UtilUser.token
        if (token != null) {
            if (offerId != null) {
                offer_id = offerId
                view.showProgressDialog()
                offerService.getOfferDetail(token, offerId)
            }
        }
    }

    fun userClicked() {
        val intent = Intent()
        intent.setClass(context, OtherView::class.java)
        intent.putExtra("user_id", user_id)
        context.startActivity(intent)
    }

    fun chatButtonClicked() {
        val token = UtilUser.token
        if (token != null) {
            if (user_id != null) {
                view.showProgressDialog()
                val request = ChatRequest(
                    user1_id = UtilUser.id.toString(),
                    user2_id = user_id.toString(),
                    offer_id = offer_id.toString()
                )
                chatService.createChat(request, token)
            }
        }
    }

    fun deleteButtonPressed() {
        view.askDelete()
    }

    fun editButtonPressed() {
        val intent = Intent()
        intent.setClass(context, CreateOfferView::class.java)
        intent.putExtra("edit", "1")
        if (offerInfo != null) {
            intent.putExtra("title", offerInfo!!.title)
            intent.putExtra("description", offerInfo!!.description)
            intent.putExtra("online", offerInfo!!.online)
            intent.putExtra("location", offerInfo!!.location)
            intent.putExtra("totalPrice", offerInfo!!.total_price)
            intent.putExtra("hourPrice", offerInfo!!.hour_price)
            intent.putExtra("image", offerInfo!!.image)
            intent.putExtra("offerId", offerInfo!!.id)
        }
        context.startActivity(intent)
    }

    fun deleteOffer() {
        val token = UtilUser.token
        if (token != null) {
            if (offer_id != null) {
                view.showProgressDialog()
                offerService.deleteOffer(token, offer_id!!)
            }
        }
    }

    fun isMyOffer(): Boolean {
        if (user_id != null && UtilUser.id != null) {
            return user_id == UtilUser.id
        }
        return false
    }

    fun nextButtonPressed() {
        if (index < array_images.size - 1) {
            index++
            if (array_images[index].image != null) {
                view.showImage(array_images[index].image!!)
                view.updateCount(index + 1, array_images.size)
            }
        }
    }

    fun previousButtonPressed() {
        if (index > 0) {
            index--
            if (array_images[index].image != null) {
                view.showImage(array_images[index].image!!)
                view.updateCount(index + 1, array_images.size)
            }
        }
    }

    private fun handleResponse(code: Int, data: OfferDetailResponse?) {
        when (code) {
            in 200..299 -> {
                if (data != null) {
                    user_id = data.data.id_user
                    user_name = data.data.name_user
                    user_image = data.data.image_user
                    offerInfo = data.data
                    array_images = data.data.images
                    view.showOfferDetail(data.data)
                }
            }
        }
    }

    private fun handleChatResponse(code: Int) {
        when (code) {
            in 200..299 -> {
                val intent = Intent()
                intent.setClass(context, ChatView::class.java)
                intent.putExtra("userId", user_id)
                intent.putExtra("userName", user_name)
                intent.putExtra("userImage", user_image)
                intent.putExtra("chatId", "0")
                context.startActivity(intent)
            }
            else -> {
                val intent = Intent()
                intent.setClass(context, ChatView::class.java)
                intent.putExtra("userId", user_id)
                intent.putExtra("userName", user_name)
                intent.putExtra("userImage", user_image)
                intent.putExtra("chatId", "0")
                context.startActivity(intent)
            }
        }
    }

    fun buyButtonPressed() {
        val intent = Intent()
        intent.setClass(context, BuyView::class.java)
        context.startActivity(intent)
    }

    private fun goToMyOffers() {
        val intent = Intent()
        intent.setClass(context, HomeView::class.java)
        intent.putExtra("view", "2")
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        context.startActivity(intent)
    }

    // Override

    override fun onOfferDetailCompleted(response: Pair<Int, OfferDetailResponse?>) {
        view.closeDialog()
        handleResponse(response.first, response.second)
    }

    override fun onChatCreatedCompleted(code: Int) {
        view.closeDialog()
        handleChatResponse(code)
    }

    override fun onDeleteOfferCompleted(code: Int) {
        view.closeDialog()
        goToMyOffers()
    }

    override fun onGetListChatsCompleted(response: Pair<Int, ListChatsResponse?>) {}

    override fun onMyOffersCompleted(response: Pair<Int, MyOffersResponse?>) {}

    override fun onOfferCreatedCompleted(code: Int) {}

    override fun onOffersCompleted(response: Pair<Int, OffersResponse?>) {}

    override fun onChatDeleteCompleted(code: Int) {}

    override fun onImagesCompleted(response: Pair<Int, ImagesResponse?>) {}

}