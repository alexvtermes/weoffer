package com.upc.tfm.weoffer

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.upc.tfm.weoffer.home.HomeView
import com.upc.tfm.weoffer.login.LoginView
import com.upc.tfm.weoffer.storage.InternalStorage
import java.io.File

class MainActivity : AppCompatActivity() {

    private val internalStorage: InternalStorage

    init {
        internalStorage = InternalStorage()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initApplicationLogic()
    }

    fun initApplicationLogic() {
        val dir: File = this.filesDir
        val file_type: File = File(dir, "userfile")
        if (file_type.exists()) {
            startActivity(Intent(this, HomeView::class.java))
        }
        else {
            startActivity(Intent(this, LoginView::class.java))
        }
        finish()
    }

}