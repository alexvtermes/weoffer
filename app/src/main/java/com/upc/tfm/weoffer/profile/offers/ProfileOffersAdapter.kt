package com.upc.tfm.weoffer.profile.offers

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.upc.tfm.weoffer.R
import com.upc.tfm.weoffer.network.MyOffersResponse
import com.upc.tfm.weoffer.offerdetail.OfferDetailView
import com.upc.tfm.weoffer.profile.ProfilePresenter
import kotlinx.android.synthetic.main.profile_offer_item.view.*

class ProfileOffersAdapter(val context: Context?, var list: MyOffersResponse): RecyclerView.Adapter<ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.title.text = list.data[position].title
        holder.price.text = list.data[position].hour_price
        holder.image.setImageBitmap(getImage(list.data[position].image))
        holder.image.setOnClickListener {
            val intent = Intent()
            if (context != null) {
                intent.setClass(context, OfferDetailView::class.java)
                intent.putExtra("offer_id", list.data[position].id)
                context.startActivity(intent)
            }
        }
    }

    private fun getImage(image: String?): Bitmap? {
        val encodedString = image
        if (encodedString != null) {
            val encoded = encodedString.split(",")
            val decodedString = Base64.decode(encoded[1], Base64.DEFAULT)
            return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
        }
        return null
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.profile_offer_item,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.data.size
    }

}


class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {
    val title = view.profile_offer_title
    val price = view.profile_offer_price
    val image = view.profile_offer_image
}