package com.upc.tfm.weoffer.profile.reviews

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.upc.tfm.weoffer.R
import com.upc.tfm.weoffer.network.MyOffersResponse
import com.upc.tfm.weoffer.network.ReviewsResponse
import com.upc.tfm.weoffer.profile.offers.ProfileOffersAdapter
import kotlinx.android.synthetic.main.profile_offer_fragment.*
import kotlinx.android.synthetic.main.profile_offer_fragment.profile_empty_view
import kotlinx.android.synthetic.main.profile_review_fragment.*

class ProfileReviewsFragment: Fragment() {

    private lateinit var itemAdapter: ProfileReviewsAdapter
    private var other: Boolean = false


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.profile_review_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val bundle = arguments
        if (bundle != null) {
            val list: ReviewsResponse = bundle.getSerializable("Object") as ReviewsResponse
            initRecyclerView(list)
        }
    }

    private fun initRecyclerView(list: ReviewsResponse?) {
        if (list != null) {
            if (recycler_reviews != null) {
                recycler_reviews.layoutManager = LinearLayoutManager(context)
                itemAdapter = ProfileReviewsAdapter(context, list)
                recycler_reviews.adapter = itemAdapter
                if (list.data.isEmpty()) {
                    profile_empty_view.visibility = View.VISIBLE
                }
                else profile_empty_view.visibility = View.GONE
            }
        }
    }

    fun reloadRecyclerView(reviews: ReviewsResponse?) {
        if (reviews != null) {
            itemAdapter.updateData(reviews)
            itemAdapter.notifyDataSetChanged()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
    }

}