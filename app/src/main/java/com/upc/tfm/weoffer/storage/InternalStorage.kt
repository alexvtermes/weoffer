package com.upc.tfm.weoffer.storage

import android.content.Context
import com.upc.tfm.weoffer.login.LoginResponse
import com.upc.tfm.weoffer.utils.UtilUser
import java.io.BufferedReader
import java.io.File
import java.io.InputStreamReader

class InternalStorage {

    private val filename_user = "userfile"

    fun storeUser(context: Context, body: LoginResponse) {
        try {
            File(context.filesDir, filename_user).bufferedWriter()
            val file = context.openFileOutput(filename_user, Context.MODE_PRIVATE).bufferedWriter()
            file.write(body.name)
            file.newLine()
            file.write(body.surname)
            file.newLine()
            file.write(body.gender)
            file.newLine()
            file.write(body.birthday)
            file.newLine()
            file.write(body.email)
            file.newLine()
            file.write(body.country)
            file.newLine()
            file.write(body.token)
            file.newLine()
            file.write(body.id)
            file.newLine()
            file.write(body.image)
            file.newLine()
            file.close()
        }
        catch (e: Exception) {

        }
    }

    fun getUser(context: Context): ArrayList<String>? {
        try {
            var file = context.openFileInput(filename_user)
            var isr = InputStreamReader(file)
            var br = BufferedReader(isr)
            var sb = ArrayList<String>()
            var it = 0
            var line = br.readLine()
            while (line != null) {
                sb.add(line)
                line = br.readLine()
                it += 1
                if (it == 10) break
            }
            file.close()
            isr.close()
            br.close()
            return sb
        }
        catch (e: Exception) {

        }
        return null
    }

    fun updateImage(context: Context, image: String) {
        try {
            var file = context.openFileInput(filename_user)
            var isr = InputStreamReader(file)
            var br = BufferedReader(isr)
            var sb = ArrayList<String>()
            var it = 0
            var line = br.readLine()
            while (line != null) {
                if (it == 8) {
                    sb.add(image)
                }
                else sb.add(line)
                line = br.readLine()
                it += 1
                if (it == 9) break
            }
            file.close()
            isr.close()
            br.close()
            val newInfo = LoginResponse(
                name = sb[0],
                surname = sb[1],
                gender = sb[2],
                birthday = sb[3],
                email = sb[4],
                country = sb[5],
                token = sb[6],
                id = sb[7],
                image = sb[8],
                stars = 0f,
                count = 0
            )
            deleteUser(context)
            storeUser(context, newInfo)
            UtilUser.image = image
        }
        catch (e: Exception) {

        }
    }

    fun deleteUser(context: Context): Boolean {
        val dir: File = context.filesDir
        val file: File = File(dir, filename_user)
        return file.delete()
    }

}