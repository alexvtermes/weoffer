package com.upc.tfm.weoffer.register

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.provider.MediaStore
import android.util.Base64
import androidx.core.app.ActivityCompat.startActivityForResult
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.upc.tfm.weoffer.R
import com.upc.tfm.weoffer.home.HomeView
import com.upc.tfm.weoffer.login.LoginRequest
import com.upc.tfm.weoffer.login.LoginResponse
import com.upc.tfm.weoffer.service.UserService
import com.upc.tfm.weoffer.storage.InternalStorage
import java.io.ByteArrayOutputStream
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern
import kotlin.collections.ArrayList

class RegisterPresenter(val context: Context, val view: IRegisterView): UserService.OnUserCompleted {

    private var userService: UserService
    private var internalStorage: InternalStorage

    private var countries = arrayListOf<String>()
    private var myEmail: String = ""
    private var myPassword: String = ""

    init {
        userService = UserService(this)
        val locales: Array<String> = Locale.getISOCountries()
        for (countryCode in locales)
            this.countries.add(
                Locale("", countryCode).displayCountry)
        internalStorage = InternalStorage()
    }

    fun registerButtonPressed(registerName: String, registerSurname: String, registerGender: String,
                            registerDate: String, registerCountry: String, registerEmail: String,
                            registerPassword1: String, registerPassword2: String, registerImage: Bitmap?) {
        if (!checkRequiredFields(registerName, registerSurname, registerGender, registerDate, registerCountry,
            registerEmail, registerPassword1, registerPassword2)) {
            if (isEmailValid(registerEmail)) {
                if (passwordsMatch(registerPassword1, registerPassword2)) {
                    registerUser(registerName, registerSurname, registerDate, registerGender, registerCountry,
                            registerEmail, registerPassword1, registerImage)
                }
                else view.showPasswordsNotMatch()
            }
            else view.showEmailNotValid()
        }
        else view.showMissingFields()

    }

    private fun checkRequiredFields(registerName: String, registerSurname: String, registerGender: String, registerDate: String,
    registerCountry: String, registerEmail: String, registerPassword1: String, registerPassword2: String): Boolean {
        var missingField = false
        if (registerName.isEmpty()) {
            view.showMissingName()
            missingField = true
        }
        if (registerSurname.isEmpty()) {
            view.showMissingSurname()
            missingField = true
        }
        if (registerDate.isEmpty()) {
            view.showMissingDate()
            missingField = true
        }
        if (registerCountry.isEmpty()) {
            view.showMissingCountry()
            missingField = true
        }
        if (registerEmail.isEmpty()) {
            view.showMissingEmail()
            missingField = true
        }
        if (registerPassword1.isEmpty()) {
            view.showMissingPassword1()
            missingField = true
        }
        if (registerPassword2.isEmpty()) {
            view.showMissingPassword2()
            missingField = true
        }
        return missingField
    }

    private fun registerUser(name: String, surname: String, date: String, gender: String, country: String,
                             email: String, password: String, image: Bitmap?) {
        view.showProgressDialog()
        val bos = ByteArrayOutputStream()
        image?.compress(Bitmap.CompressFormat.WEBP, 50, bos)
        var encodedImage = Base64.encodeToString(bos.toByteArray(), Base64.NO_WRAP)
        encodedImage = "data:image/png;base64," + encodedImage
        val request = RegisterRequest(
            name = name,
            surname = surname,
            email = email,
            birthday = date,
            country = country,
            gender = gender,
            password = password,
            image = encodedImage
        )
        myEmail = request.email
        myPassword = request.password
        userService.register(request)
    }

    fun getResizedBitmap(image: Bitmap, bitmapWidth: Int, bitmapHeight: Int): Bitmap {
        return Bitmap.createScaledBitmap(image, bitmapWidth, bitmapHeight, true)
    }

    private fun isEmailValid(email: String?): Boolean {
        val expression: String = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$"
        val pattern: Pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE)
        val matcher: Matcher = pattern.matcher(email)
        return matcher.matches()
    }

    private fun passwordsMatch(password1: String, password2: String): Boolean {
        return password1 == password2
    }

    fun getCountries(): ArrayList<String> {
        return countries
    }

    fun getGenders(): Array<String> {
        return context.resources.getStringArray(R.array.register_gender)
    }

    private fun handleResponse(code: Int): Boolean {
        when (code) {
            in 200..299 -> {
                view.showProgressDialog()
                view.showToast(context.getString(R.string.correctly_registered))
                val request = LoginRequest(
                    email = myEmail,
                    password = myPassword
                )
                userService.doLogin(request)
                return true
            }
            400 -> view.showToast(context.getString(R.string.register_user_exists))
            else -> view.showToast(context.getString(R.string.internal_server_error))
        }
        return false
    }

    private fun goToHomeView() {
        val intent = Intent()
        intent.setClass(context, HomeView::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        context.startActivity(intent)
    }

    private fun handleLogin(response: Pair<Int, LoginResponse?>) {
        when (response.first) {
            in 200..299 -> {
                if (response.second != null) {
                    internalStorage.storeUser(context, response.second!!)
                    goToHomeView()
                }
            }
        }
    }

    fun imageClicked() {
        val galleryIntent = Intent(
            Intent.ACTION_PICK,
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        )
        startActivityForResult(
            context as Activity,
            galleryIntent,
            1,
            null
        )
    }

    //Override

    override fun onRegisterCompleted(code: Int) {
        view.closeDialog()
        handleResponse(code)
    }

    override fun onLoginCompleted(response: Pair<Int, LoginResponse?>) {
        view.closeDialog()
        handleLogin(response)
    }

    override fun onImageUpdated(code: Int) {}

    override fun onOtherUserInfoCompleted(response: Pair<Int, LoginResponse?>) {}

}