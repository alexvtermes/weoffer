package com.upc.tfm.weoffer.service

import com.upc.tfm.weoffer.createoffer.CreateOfferRequest
import com.upc.tfm.weoffer.network.*
import com.upc.tfm.weoffer.searchoffer.SearchOfferRequest
import com.upc.tfm.weoffer.utils.UtilService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class OfferService(private val delegate: OfferService.OnOfferCompleted) {

    interface OnOfferCompleted {
        fun onOfferCreatedCompleted(code: Int)
        fun onMyOffersCompleted(response: Pair<Int, MyOffersResponse?>)
        fun onOfferDetailCompleted(response: Pair<Int, OfferDetailResponse?>)
        fun onOffersCompleted(response: Pair<Int, OffersResponse?>)
        fun onDeleteOfferCompleted(code: Int)
        fun onImagesCompleted(response: Pair<Int, ImagesResponse?>)
    }

    private var callbackCreateOffer: Call<Unit>? = null
    private var callbackMyOffers: Call<MyOffersResponse>? = null
    private var callbackOfferDetail: Call<OfferDetailResponse>? = null
    private var callbackOffers: Call<OffersResponse>? = null
    private var callbackDelete: Call<Unit>? = null
    private var callbackSearchOffer: Call<OffersResponse>? = null
    private var callbackImages: Call<ImagesResponse>? = null

    fun createOffer(request: CreateOfferRequest, token: String) {
        val retrofit = UtilService.prepareRetrofit()
        val service = retrofit.create(OfferAPI::class.java)
        UtilService.setPolicy()
        val header: MutableMap<String, String> = HashMap()
        header["Authorization"] = token
        callbackCreateOffer = service.createOffer(header, request)
        callbackCreateOffer?.enqueue(object: Callback<Unit> {
            override fun onFailure(call: Call<Unit>?, t: Throwable) {
                if (call != null && call.isCanceled) {
                } else if (t is Exception) {
                    callbackCreateOffer = null
                    delegate.onOfferCreatedCompleted(900)
                }
                callbackCreateOffer = null
            }

            override fun onResponse(call: Call<Unit>, response: Response<Unit>?) {
                if (response != null)
                    delegate.onOfferCreatedCompleted(response.code())
                callbackCreateOffer = null

            }
        })
    }

    fun getMyOffers(token: String) {
        val retrofit = UtilService.prepareRetrofit()
        val service = retrofit.create(OfferAPI::class.java)
        UtilService.setPolicy()
        val header: MutableMap<String, String> = HashMap()
        header["Authorization"] = token
        callbackMyOffers = service.getMyOffers(header)
        callbackMyOffers?.enqueue(object: Callback<MyOffersResponse>
        {
            override fun onFailure(call: Call<MyOffersResponse>?, t: Throwable) {
                if (call != null && call.isCanceled)
                {}
                else if (t is Exception)
                {
                    callbackMyOffers = null
                    delegate.onMyOffersCompleted(Pair(900, null))
                }
                callbackMyOffers = null
            }

            override fun onResponse(call: Call<MyOffersResponse>, response: Response<MyOffersResponse>?) {
                if (response != null)
                    delegate.onMyOffersCompleted(Pair(response.code(), response.body()))
                callbackMyOffers = null
            }
        })
    }

    fun getOfferDetail(token: String, offerId: String) {
        val retrofit = UtilService.prepareRetrofit()
        val service = retrofit.create(OfferAPI::class.java)
        UtilService.setPolicy()
        val header: MutableMap<String, String> = HashMap()
        header["Authorization"] = token
        val url = "/offer/detail/" + offerId
        callbackOfferDetail = service.getOfferDetail(url, header)
        callbackOfferDetail?.enqueue(object: Callback<OfferDetailResponse>
        {
            override fun onFailure(call: Call<OfferDetailResponse>?, t: Throwable) {
                if (call != null && call.isCanceled)
                {
                    //do nothing view does no exists anymore
                }
                else if (t is Exception)
                {
                    callbackOfferDetail = null
                    delegate.onOfferDetailCompleted(Pair(900, null))
                }
                callbackOfferDetail = null
            }

            override fun onResponse(call: Call<OfferDetailResponse>, response: Response<OfferDetailResponse>?) {
                if (response != null)
                    delegate.onOfferDetailCompleted(Pair(response.code(), response.body()))
                callbackOfferDetail = null
            }
        })
    }

    fun getOffers(token: String, request: SearchOfferRequest) {
        val retrofit = UtilService.prepareRetrofit()
        val service = retrofit.create(OfferAPI::class.java)
        UtilService.setPolicy()
        val header: MutableMap<String, String> = HashMap()
        header["Authorization"] = token
        callbackOffers = service.searchOffer(header, request)
        callbackOffers?.enqueue(object: Callback<OffersResponse>
        {
            override fun onFailure(call: Call<OffersResponse>?, t: Throwable) {
                if (call != null && call.isCanceled)
                {}
                else if (t is Exception)
                {
                    callbackOffers = null
                    delegate.onOffersCompleted(Pair(900, null))
                }
                callbackOffers = null
            }

            override fun onResponse(call: Call<OffersResponse>, response: Response<OffersResponse>?) {
                if (response != null)
                    delegate.onOffersCompleted(Pair(response.code(), response.body()))
                callbackOffers = null
            }
        })
    }

    fun getOtherOffers(token: String, user_id: String) {
        val retrofit = UtilService.prepareRetrofit()
        val service = retrofit.create(OfferAPI::class.java)
        UtilService.setPolicy()
        val header: MutableMap<String, String> = HashMap()
        header["Authorization"] = token
        val url = "/offer/user/" + user_id
        callbackMyOffers = service.getOtherOffers(url, header)
        callbackMyOffers?.enqueue(object: Callback<MyOffersResponse>
        {
            override fun onFailure(call: Call<MyOffersResponse>?, t: Throwable) {
                if (call != null && call.isCanceled)
                {}
                else if (t is Exception)
                {
                    callbackMyOffers = null
                    delegate.onMyOffersCompleted(Pair(900, null))
                }
                callbackMyOffers = null
            }

            override fun onResponse(call: Call<MyOffersResponse>, response: Response<MyOffersResponse>?) {
                if (response != null)
                    delegate.onMyOffersCompleted(Pair(response.code(), response.body()))
                callbackMyOffers = null
            }
        })
    }

    fun deleteOffer(token: String, offerId: String) {
        val retrofit = UtilService.prepareRetrofit()
        val service = retrofit.create(OfferAPI::class.java)
        UtilService.setPolicy()
        val header: MutableMap<String, String> = HashMap()
        header["Authorization"] = token
        val url = "/offer/" + offerId
        callbackDelete = service.deleteOffer(url, header)
        callbackDelete?.enqueue(object: Callback<Unit>
        {
            override fun onFailure(call: Call<Unit>?, t: Throwable) {
                if (call != null && call.isCanceled)
                {
                    //do nothing view does no exists anymore
                }
                else if (t is Exception)
                {
                    callbackDelete = null
                    delegate.onDeleteOfferCompleted(900)
                }
                callbackDelete = null
            }

            override fun onResponse(call: Call<Unit>, response: Response<Unit>?) {
                if (response != null)
                    delegate.onDeleteOfferCompleted(response.code())
                callbackDelete = null
            }
        })
    }

    fun editOffer(request: CreateOfferRequest, token: String, offerId: String) {
        val retrofit = UtilService.prepareRetrofit()
        val service = retrofit.create(OfferAPI::class.java)
        UtilService.setPolicy()
        val header: MutableMap<String, String> = HashMap()
        header["Authorization"] = token
        val url = "/offer/edit/" + offerId
        callbackCreateOffer = service.editOffer(url, header, request)
        callbackCreateOffer?.enqueue(object: Callback<Unit> {
            override fun onFailure(call: Call<Unit>?, t: Throwable) {
                if (call != null && call.isCanceled) {
                } else if (t is Exception) {
                    callbackCreateOffer = null
                    delegate.onOfferCreatedCompleted(900)
                }
                callbackCreateOffer = null
            }

            override fun onResponse(call: Call<Unit>, response: Response<Unit>?) {
                if (response != null)
                    delegate.onOfferCreatedCompleted(response.code())
                callbackCreateOffer = null

            }
        })
    }

    fun searchOffer(request: SearchOfferRequest, token: String) {
        val retrofit = UtilService.prepareRetrofit()
        val service = retrofit.create(OfferAPI::class.java)
        UtilService.setPolicy()
        val header: MutableMap<String, String> = HashMap()
        header["Authorization"] = token
        callbackSearchOffer = service.searchOffer(header, request)
        callbackSearchOffer?.enqueue(object: Callback<OffersResponse> {
            override fun onFailure(call: Call<OffersResponse>?, t: Throwable) {
                if (call != null && call.isCanceled) {
                } else if (t is Exception) {
                    callbackSearchOffer = null
                    delegate.onOffersCompleted(Pair(900, null))
                }
                callbackSearchOffer = null
            }

            override fun onResponse(call: Call<OffersResponse>, response: Response<OffersResponse>?) {
                if (response != null)
                    delegate.onOffersCompleted(Pair(response.code(), response.body()))
                callbackSearchOffer = null

            }
        })
    }

    fun getImages(token: String, offerId: String) {
        val retrofit = UtilService.prepareRetrofit()
        val service = retrofit.create(OfferAPI::class.java)
        UtilService.setPolicy()
        val header: MutableMap<String, String> = HashMap()
        header["Authorization"] = token
        val url = "/images/get/" + offerId
        callbackImages = service.getImages(url, header)
        callbackImages?.enqueue(object: Callback<ImagesResponse>
        {
            override fun onFailure(call: Call<ImagesResponse>?, t: Throwable) {
                if (call != null && call.isCanceled)
                {}
                else if (t is Exception)
                {
                    callbackImages = null
                    delegate.onImagesCompleted(Pair(900, null))
                }
                callbackImages = null
            }

            override fun onResponse(call: Call<ImagesResponse>, response: Response<ImagesResponse>?) {
                if (response != null)
                    delegate.onImagesCompleted(Pair(response.code(), response.body()))
                callbackImages = null
            }
        })
    }

}