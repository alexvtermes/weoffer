package com.upc.tfm.weoffer.searchoffer

interface ISearchOfferView {

    fun showProgressDialog()
    fun closeDialog()
    fun showOnlineFaceError()

}