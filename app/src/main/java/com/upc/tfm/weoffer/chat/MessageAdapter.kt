package com.upc.tfm.weoffer.chat

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.auth.FirebaseUser
import com.upc.tfm.weoffer.R
import com.upc.tfm.weoffer.utils.UtilUser
import kotlinx.android.synthetic.main.chat_item_right.view.*


class MessageAdapter(context: Context, listchats: ArrayList<Chat>): RecyclerView.Adapter<MessageAdapter.ViewHolder>() {

    private val MSG_TYPE_LEFT = 0
    private val MSG_TYPE_RIGHT = 1
    private var listchat = listchats
    private var fuser: FirebaseUser? = null
    private val mContext = context


    override fun getItemCount(): Int {
        return listchat.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.message?.text = listchat[position].message
        holder.time?.text = listchat[position].time
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        if (viewType == MSG_TYPE_RIGHT) {
            return MessageAdapter.ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.chat_item_right, parent, false))
        }
        else
            return MessageAdapter.ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.chat_item_left, parent, false))
    }


    class ViewHolder(view: View): RecyclerView.ViewHolder(view) {
        val message = view.show_message
        val time = view.show_time
    }

    override fun getItemViewType(position: Int): Int {
        if (listchat[position].sender == UtilUser.id)
            return MSG_TYPE_RIGHT
        else
            return MSG_TYPE_LEFT
    }

}