package com.upc.tfm.weoffer.profile

import android.app.Dialog
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentActivity
import com.upc.tfm.weoffer.R
import com.upc.tfm.weoffer.login.LoginResponse
import com.upc.tfm.weoffer.network.ReviewsResponse
import kotlinx.android.synthetic.main.home_view.*

class OtherView: AppCompatActivity(), IProfileView {

    private lateinit var presenter: ProfilePresenter
    private var dialog: Dialog? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter = ProfilePresenter(this, this)
        setContentView(R.layout.other_profile_view)
        init()
    }

    private fun init() {
        initToolbar()
        val userId = intent.getStringExtra("user_id")
        var bundle = Bundle()
        bundle.putString("other", "1")
        bundle.putString("user_id", userId.toString())
        val fragment = ProfileView()
        fragment.arguments = bundle
        (this as FragmentActivity).supportFragmentManager
            .beginTransaction()
            .setCustomAnimations(R.animator.fade_in, R.animator.fade_out)
            .replace(R.id.other_frameFragment, fragment)
            .commit()
    }

    private fun initToolbar() {
        setSupportActionBar(home_toolbar)
        if (supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun closeDialog() {
        dialog?.dismiss()
    }

    override fun showProgressDialog() {}

    override fun setOffersData(data: Bundle) {}

    override fun setOtherUserData(data: LoginResponse) {}

    override fun showMessage(msg: String) {}

    override fun setReviewsData(data: Bundle) {}

    override fun setReviewCountData(count: Int, stars: Float) {}

}