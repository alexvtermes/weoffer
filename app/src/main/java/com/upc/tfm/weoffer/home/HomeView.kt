package com.upc.tfm.weoffer.home

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Base64
import android.view.Menu
import android.view.MenuItem
import android.widget.SearchView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import com.upc.tfm.weoffer.R
import com.upc.tfm.weoffer.listchats.ListChatsView
import com.upc.tfm.weoffer.myoffers.MyOffersView
import com.upc.tfm.weoffer.offers.OffersView
import com.upc.tfm.weoffer.profile.ProfileView
import com.upc.tfm.weoffer.storage.InternalStorage
import com.upc.tfm.weoffer.utils.UtilUser
import kotlinx.android.synthetic.main.home_view.*
import kotlinx.android.synthetic.main.nav_header_main.view.*

class HomeView: AppCompatActivity(), IHomeView {

    private var presenter: HomePresenter
    private var internalStorage: InternalStorage

    init {
        presenter = HomePresenter(this, this)
        internalStorage = InternalStorage()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.home_view)

        init()
    }

    private fun init() {
        initFragment()
        initUserInfo()
        initHeader()
        initMenu()
    }

    private fun initFragment()
    {
        val part: String? = intent.getStringExtra("view")
        if (part == null) {
            supportFragmentManager
                .beginTransaction()
                .add(R.id.home_frameFragment, OffersView())
                .commit()
            home_navDrawable.menu.getItem(1).isChecked = true
        }
        else if (part == "1") {
            supportFragmentManager
                .beginTransaction()
                .add(R.id.home_frameFragment, ProfileView())
                .commit()
            home_navDrawable.menu.getItem(0).isChecked = true
        }
        else if (part == "2") {
            supportFragmentManager
                .beginTransaction()
                .add(R.id.home_frameFragment, MyOffersView())
                .commit()
            home_navDrawable.menu.getItem(2).isChecked = true
        }
        else if (part == "3") {
            supportFragmentManager
                .beginTransaction()
                .add(R.id.home_frameFragment, ListChatsView())
                .commit()
            home_navDrawable.menu.getItem(3).isChecked = true
        }
    }

    private fun initUserInfo() {
        val userInfo = internalStorage.getUser(this)
        presenter.initUserInfo(userInfo)
    }

    private fun initHeader() {
        home_navDrawable.getHeaderView(0).nav_name.text = UtilUser.name + " " + UtilUser.surname
        home_navDrawable.getHeaderView(0).nav_email.text = UtilUser.email
        val bitmap = getPhotoBitmap(UtilUser.image)
        if (bitmap != null)
            home_navDrawable.getHeaderView(0).nav_image.setImageBitmap(bitmap)
    }

    private fun initMenu()
    {
        val toggle = ActionBarDrawerToggle(
            this, home_drawerLayout, home_toolbar, R.string.navigation_drawer_open,
            R.string.navigation_drawer_close)
        home_drawerLayout.addDrawerListener(toggle)
        toggle.syncState()
        home_navDrawable.setNavigationItemSelectedListener(presenter)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_drawer, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return true
    }

    override fun onBackPressed()
    {
        if (home_drawerLayout.isDrawerOpen(GravityCompat.START))
            home_drawerLayout.closeDrawer(GravityCompat.START)
        else super.onBackPressed()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1 && resultCode == Activity.RESULT_OK)
            supportFragmentManager.findFragmentById(R.id.home_frameFragment)?.onActivityResult(requestCode, resultCode, data)
    }

    private fun getPhotoBitmap(encodedString: String?): Bitmap? {
        if (encodedString != null) {
            var encoded = encodedString?.split(",")
            val decodedString = Base64.decode(encoded[1], Base64.DEFAULT)
            return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
        }
        else
            return null
    }

    //Override

    override fun closeDrawable() {
        home_drawerLayout.closeDrawer(GravityCompat.START)
    }

    override fun setTitle(title: Int) {
        home_toolbar.title = getString(title)
    }

}