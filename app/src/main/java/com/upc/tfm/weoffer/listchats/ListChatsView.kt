package com.upc.tfm.weoffer.listchats

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.upc.tfm.weoffer.R
import com.upc.tfm.weoffer.chat.ChatRequest
import com.upc.tfm.weoffer.myoffers.OnItemClickListener
import com.upc.tfm.weoffer.myoffers.addOnItemClickListener
import com.upc.tfm.weoffer.network.ListChatsResponse
import kotlinx.android.synthetic.main.chat_view.*
import kotlinx.android.synthetic.main.list_chats_view.*
import kotlinx.android.synthetic.main.list_chats_view.empty_view
import kotlinx.android.synthetic.main.offers_view.*

class ListChatsView: Fragment(), IListChatsView, SwipeRefreshLayout.OnRefreshListener {

    private lateinit var presenter: ListChatsPresenter
    private lateinit var adapter: ListChatAdapter
    private lateinit var activity: Activity
    private var dialog: Dialog? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.list_chats_view, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)
        (activity as AppCompatActivity).supportActionBar?.title = activity.getString(R.string.list_chat_title)
        init()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu?.clear()
        inflater?.inflate(R.menu.main_drawer, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    private fun init() {
        presenter = ListChatsPresenter(activity, this)
        presenter.getListChats()
        initRefreshLayout()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        activity = context as (Activity)
    }

    private fun initRecyclerView(list: List<ListChatsRequest>) {
        list_chats_recycler_view.layoutManager = LinearLayoutManager(context)
        adapter = ListChatAdapter(context!!, list, presenter)
        list_chats_recycler_view.adapter = adapter
        emptyRecyclerLogic(list)
        list_chats_recycler_view.addOnItemClickListener(object : OnItemClickListener {
            override fun onItemClicked(position: Int, view: View) {
                presenter.itemClicked(position)
            }
        })
    }

    private fun emptyRecyclerLogic(list_chats: List<ListChatsRequest>) {
        if (list_chats.isEmpty()) {
            list_chats_recycler_view.visibility = View.GONE
            empty_view.visibility = View.VISIBLE
            empty_view.text = context!!.getString(R.string.list_chats_empty)
        } else {
            list_chats_recycler_view.visibility = View.VISIBLE
            empty_view.visibility = View.GONE
        }
    }

    private fun initRefreshLayout() {
        list_chats_reloadLayout.setOnRefreshListener(this)
        list_chats_reloadLayout.setColorSchemeResources(R.color.colorPrimary)
    }

    // Override

    override fun showProgressDialog() {
        if (context != null) {
            dialog = Dialog(context!!)
            dialog?.setContentView(R.layout.progressbar_layout)
            dialog?.setCancelable(false)
            dialog?.setCanceledOnTouchOutside(false)
            dialog?.show()
        }
    }

    override fun closeDialog() {
        dialog?.dismiss()
    }

    override fun setChats(data: List<ListChatsRequest>) {
        initRecyclerView(data)
    }

    override fun onRefresh() {
        presenter.getListChats()
        list_chats_reloadLayout.isRefreshing = false
    }

}