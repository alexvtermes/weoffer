package com.upc.tfm.weoffer.listchats

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.database.*
import com.upc.tfm.weoffer.R
import com.upc.tfm.weoffer.chat.Chat
import com.upc.tfm.weoffer.utils.UtilUser
import kotlinx.android.synthetic.main.list_chat_item.view.*


class ListChatAdapter(val context: Context, var chats: List<ListChatsRequest>, val presenter: ListChatsPresenter) : RecyclerView.Adapter<ViewHolder>() {

    private lateinit var reference: DatabaseReference


    fun getUserPhoto(image: String?): Bitmap? {
        val encodedString = image
        if (encodedString != null) {
            val encoded = encodedString.split(",")
            val decodedString = Base64.decode(encoded[1], Base64.DEFAULT)
            return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
        }
        return null
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.name?.text = chats[position].user_name + " " + chats[position].user_surname
        val image = getUserPhoto(chats[position].user_image)
        handleLastMessages(holder, position)
        if (image != null) {
            holder.image?.setImageDrawable(BitmapDrawable(context.resources, image))
            holder.image?.setImageBitmap(image)
        }
    }

    override fun getItemCount(): Int {
        return chats.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.list_chat_item, parent, false))
    }

    fun updateData(data: List<ListChatsRequest>) {
        chats = data
    }

    private fun handleLastMessages(holder: ViewHolder, position: Int) {
        reference = FirebaseDatabase.getInstance().getReference("Chats")
        var lastmessage = ""
        reference.addValueEventListener(object: ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                for (snapshot in dataSnapshot.children) {
                    val chat: Chat? = snapshot.getValue(Chat::class.java)
                    if ((chat!!.receiver == UtilUser.id && chat.sender == chats[position].user_id) || (chat.receiver == chats[position].user_id && chat.sender == UtilUser.id)) {
                        chats[position].last_message = chat.message
                        lastmessage = chat.message
                    }
                }
                holder.lastMessage.text = lastmessage
            }
            override fun onCancelled(p0: DatabaseError) {
            }
        })
    }

}

class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {
    val name = view.list_chats_name
    val image = view.list_chats_image
    val lastMessage = view.list_chats_last_message
}