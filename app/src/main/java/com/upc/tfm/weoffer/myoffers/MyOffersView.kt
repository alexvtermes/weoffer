package com.upc.tfm.weoffer.myoffers

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.upc.tfm.weoffer.R
import kotlinx.android.synthetic.main.my_offers_view.*


class MyOffersView: Fragment(), IMyOffersView {

    private lateinit var presenter: MyOffersPresenter
    private lateinit var adapter: MyOffersAdapter
    private lateinit var activity: Activity
    private var dialog: Dialog? = null
    private var first: Boolean = true


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.my_offers_view, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)
        (activity as AppCompatActivity).supportActionBar?.title = activity.getString(R.string.my_offers_title)
        init()
    }

    override fun onResume() {
        super.onResume()
        if (!first) {
            presenter.getMyOffers()
        }
        first = false
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu?.clear()
        inflater?.inflate(R.menu.main_drawer, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    private fun init() {
        presenter = MyOffersPresenter(activity, this)
        presenter.getMyOffers()
        initFloatingButton()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        activity = context as (Activity)
    }

    private fun initFloatingButton() {
        fab.setOnClickListener {
            presenter.addButtonPressed()
        }
    }

    private fun initRecyclerView(myOffers: List<MyOfferRequest>?) {
        if (myOffers != null) {
            my_offers_recyclerView.layoutManager = LinearLayoutManager(context)
            adapter = MyOffersAdapter(context!!, myOffers, presenter)
            my_offers_recyclerView.adapter = adapter
            emptyRecyclerLogic(myOffers)
            my_offers_recyclerView.addOnItemClickListener(object : OnItemClickListener {
                override fun onItemClicked(position: Int, view: View) {
                    presenter.itemClicked(position)
                }
            })
        }
    }

    private fun emptyRecyclerLogic(myOffers: List<MyOfferRequest>) {
        if (myOffers.isEmpty()) {
            my_offers_recyclerView.visibility = View.GONE
            empty_view.visibility = View.VISIBLE
        } else {
            my_offers_recyclerView.visibility = View.VISIBLE
            empty_view.visibility = View.GONE
        }
    }

    // Overrides

    override fun showToast(msg: String) {
        Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show()
    }

    override fun reloadRecyclerView(myOffers: List<MyOfferRequest>) {
        adapter.updateData(myOffers)
        adapter.notifyDataSetChanged()
        emptyRecyclerLogic(myOffers)
    }

    override fun showProgressDialog() {
        if (context != null) {
            dialog = Dialog(context!!)
            dialog?.setContentView(R.layout.progressbar_layout)
            dialog?.setCancelable(false)
            dialog?.setCanceledOnTouchOutside(false)
            dialog?.show()
        }
    }

    override fun closeDialog() {
        dialog?.dismiss()
    }

    override fun setMyOffers(data: List<MyOfferRequest>) {
        initRecyclerView(data)
    }

}