package com.upc.tfm.weoffer.listchats

import android.content.Context
import android.content.Intent
import com.upc.tfm.weoffer.chat.ChatRequest
import com.upc.tfm.weoffer.chat.ChatView
import com.upc.tfm.weoffer.network.ListChatsResponse
import com.upc.tfm.weoffer.service.ChatService
import com.upc.tfm.weoffer.utils.UtilUser
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class ListChatsPresenter(val context: Context, val view: IListChatsView): ChatService.OnChatCompleted {

    private val chatService: ChatService = ChatService(this)
    private var mListChats: MutableList<ListChatsRequest> = ArrayList()

    fun getListChats() {
        val token = UtilUser.token
        if (token != null) {
            view.showProgressDialog()
            chatService.getListChats(token)
        }
    }

    private fun orderList() {
        val dateTimeStrToLocalDateTime: (String) -> LocalDateTime = {
            LocalDateTime.parse(it, DateTimeFormatter.ofPattern("dd/MM/yyyy | HH:mm:ss"))
        }
        val list = ArrayList<String>()
        for (chat in mListChats) {
            list.add(chat.date + " | " + chat.time)
        }
        list.sortByDescending(dateTimeStrToLocalDateTime)
        val finalList: MutableList<ListChatsRequest> = ArrayList()
        for (item in list) {
            for (chat in mListChats) {
                val aux = item.split(" ")
                if (aux[2] == chat.time && aux[0] == chat.date)
                    finalList.add(chat)
            }
        }
        mListChats = finalList
    }

    private fun handleResponse(code: Int, data: ListChatsResponse?) {
        when (code) {
            in 200..299 -> {
                if (data != null) {
                    mListChats = data.data as MutableList<ListChatsRequest>
                    orderList()
                    view.setChats(mListChats)
                }
            }
        }
    }

    fun itemClicked(position: Int) {
        val userId = mListChats[position].user_id
        val intent = Intent()
        intent.setClass(context, ChatView::class.java)
        intent.putExtra("userId", userId)
        intent.putExtra("userName", mListChats[position].user_name + " " + mListChats[position].user_surname)
        intent.putExtra("userImage", mListChats[position].user_image)
        intent.putExtra("chatId", mListChats[position].id)
        context.startActivity(intent)
    }

    // Override

    override fun onGetListChatsCompleted(response: Pair<Int, ListChatsResponse?>) {
        view.closeDialog()
        handleResponse(response.first, response.second)
    }

    override fun onChatCreatedCompleted(code: Int) {}

    override fun onChatDeleteCompleted(code: Int) {}

}