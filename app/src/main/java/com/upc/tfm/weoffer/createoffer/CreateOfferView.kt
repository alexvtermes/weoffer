package com.upc.tfm.weoffer.createoffer

import android.app.ActionBar
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.util.Base64
import android.view.MenuItem
import android.view.View
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.api.Distribution
import com.upc.tfm.weoffer.R
import com.upc.tfm.weoffer.offerdetail.ImageRequest
import kotlinx.android.synthetic.main.create_offer_view.*
import kotlinx.android.synthetic.main.offer_detail_view.*
import java.io.InputStream


class CreateOfferView: AppCompatActivity(), ICreateOfferView  {

    private lateinit var presenter: CreateOfferPresenter
    private var dialog: Dialog? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.create_offer_view)

        presenter = CreateOfferPresenter(this, this)
        init()
    }

    private fun init() {
        initToolbar()
        initAddPhotoButton()
        initData()
    }

    private fun initCreateOfferButton() {
        create_offer_add_offer_button.setOnClickListener {
            presenter.createOfferButtonPressed(
                title = create_offer_title_edit.text.toString(),
                description = create_offer_description_edit.text.toString(),
                online = create_offer_online.isChecked,
                location = create_offer_location_edit.text.toString(),
                hour_total = false,
                hour_price = create_offer_hour_price.text.toString(),
                total_price = "0"
            )
        }
    }

    private fun initEditOfferButton() {
        create_offer_add_offer_button.setOnClickListener {
            presenter.editOfferButtonPressed(
                title = create_offer_title_edit.text.toString(),
                description = create_offer_description_edit.text.toString(),
                online = create_offer_online.isChecked,
                location = create_offer_location_edit.text.toString(),
                hour_total = false,
                hour_price = create_offer_hour_price.text.toString(),
                total_price = "0"
            )
        }
    }

    private fun initData() {
        val edit = intent.getStringExtra("edit")
        if (edit == "1") {
            val title = intent.getStringExtra("title")
            val description = intent.getStringExtra("description")
            val online = intent.getStringExtra("online")
            val location = intent.getStringExtra("location")
            val hourPrice = intent.getFloatExtra("hourPrice", 0f)
            val image = intent.getStringExtra("image")
            val offerId = intent.getStringExtra("offerId")
            presenter.setOfferId(offerId)

            toolbar_create_offer.setTitle(getString(R.string.offer_edit_title))
            create_offer_title_edit.setText(title)
            create_offer_description_edit.setText(description)
            create_offer_location_edit.setText(location)
            create_offer_hour_price.setText(hourPrice.toString())
            //create_offer_final_photo.setImageBitmap(imageBitmap)
            presenter.getImages(offerId)
            create_offer_main_button.text = getString(R.string.offer_edit_title)
            initEditOfferButton()
        }
        else {
            initCreateOfferButton()
        }
    }

    fun getImage(image: String?): Bitmap? {
        val encodedString = image
        if (encodedString != null) {
            val encoded = encodedString.split(",")
            val decodedString = Base64.decode(encoded[1], Base64.DEFAULT)
            return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
        }
        return null
    }

    private fun initToolbar() {
        setSupportActionBar(app_toolbar)
        if (supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        }
    }

    private fun drawableToBitmap(drawable: Drawable): Bitmap? {
        var bitmap: Bitmap? = null

        if (drawable is BitmapDrawable) {
            if (drawable.bitmap != null) {
                return drawable.bitmap
            }
        }

        if (drawable.intrinsicWidth <= 0 || drawable.intrinsicHeight <= 0) {
            bitmap = Bitmap.createBitmap(
                1,
                1,
                Bitmap.Config.ARGB_8888
            )
        } else {
            bitmap = Bitmap.createBitmap(drawable.intrinsicWidth, drawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
        }

        val canvas = Canvas(bitmap)
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight())
        drawable.draw(canvas)
        return bitmap
    }

    private fun initAddPhotoButton() {
        create_offer_add_photo.setOnClickListener {
            presenter.addPhotoButtonPressed()
        }
    }

    private fun createToast(msg: String) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }

    // Override

    override fun showMissingFields() {
        createToast(getString(R.string.register_missing_fields))
    }

    override fun showMissingTitle() {
        create_offer_title_edit.error = getString(R.string.create_offer_title_miss)
    }

    override fun showMissingDescription() {
        create_offer_description_edit.error = getString(R.string.create_offer_description_miss)
    }

    override fun showMissingLocation() {
        create_offer_location_edit.error = getString(R.string.create_offer_location_miss)
    }

    override fun showMissingHourPrice() {
        create_offer_hour_price.error = getString(R.string.create_offer_hour_price_miss)
    }

    override fun showMissingTotalPrice() {
    }

    override fun showProgressDialog() {
        dialog = Dialog(this)
        dialog?.setContentView(R.layout.progressbar_layout)
        dialog?.setCancelable(false)
        dialog?.setCanceledOnTouchOutside(false)
        dialog?.show()
    }

    override fun closeDialog() {
        dialog?.dismiss()
    }

    override fun showServerError() {
        createToast(getString(R.string.internal_server_error))
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            try {
                val imageUri = data?.data
                if (imageUri != null) {
                    val imageStream: InputStream? = contentResolver.openInputStream(imageUri)
                    val imageBitmap: Bitmap = BitmapFactory.decodeStream(imageStream)
                    val imageview = ImageView(this)
                    val params: LinearLayout.LayoutParams = LinearLayout.LayoutParams(500, 500)
                    // Image
                    imageview.setImageBitmap(imageBitmap)
                    imageview.scaleType = ImageView.ScaleType.CENTER_CROP
                    imageview.layoutParams = params
                    image_linear.addView(imageview)
                    presenter.addImage(imageBitmap)

                    // Delete Button
                    val deleteButton = ImageButton(this)
                    deleteButton.setImageDrawable(getDrawable(R.drawable.ic_delete))
                    val buttonParams: LinearLayout.LayoutParams = LinearLayout.LayoutParams(150, 130)
                    deleteButton.layoutParams = buttonParams
                    image_linear.addView(deleteButton)

                    // Line
                    val view = View(this)
                    view.layoutParams = LinearLayout.LayoutParams(
                        ActionBar.LayoutParams.MATCH_PARENT,
                        5
                    )
                    view.setBackgroundColor(Color.parseColor("#B3B3B3"))
                    image_linear.addView(view)

                    deleteButton.setOnClickListener {
                        presenter.deleteImage(imageBitmap)
                        imageview.visibility = View.GONE
                        deleteButton.visibility = View.GONE
                        view.visibility = View.GONE
                    }
                }
            } catch (exception: Exception) {
                Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show();
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun showImages(images: List<ImageRequest>?) {
        if (images != null) {
            for (i in images) {
                val imageview = ImageView(this)
                val params: LinearLayout.LayoutParams = LinearLayout.LayoutParams(
                    500,
                    500
                )
                val imageBitmap = getImage(i.image)
                imageview.setImageBitmap(imageBitmap)
                imageview.scaleType = ImageView.ScaleType.CENTER_CROP
                imageview.layoutParams = params
                image_linear.addView(imageview)
                presenter.addImage(imageBitmap)
                val deleteButton = ImageButton(this)
                deleteButton.setImageDrawable(getDrawable(R.drawable.ic_delete))
                val buttonParams: LinearLayout.LayoutParams = LinearLayout.LayoutParams(150, 130)
                deleteButton.layoutParams = buttonParams
                image_linear.addView(deleteButton)
                val view = View(this)
                view.layoutParams = LinearLayout.LayoutParams(
                    ActionBar.LayoutParams.MATCH_PARENT,
                    5
                )
                view.setBackgroundColor(Color.parseColor("#B3B3B3"))
                image_linear.addView(view)

                val viewSpace = View(this)
                viewSpace.layoutParams = LinearLayout.LayoutParams(
                    ActionBar.LayoutParams.MATCH_PARENT,
                    20
                )
                viewSpace.setBackgroundColor(Color.parseColor("#FFFFFF"))
                image_linear.addView(viewSpace)
                deleteButton.setOnClickListener {
                    presenter.deleteImage(imageBitmap)
                    imageview.visibility = View.GONE
                    deleteButton.visibility = View.GONE
                    view.visibility = View.GONE
                }
            }
        }
    }

    override fun finishCreate() {
        finish()
    }

}