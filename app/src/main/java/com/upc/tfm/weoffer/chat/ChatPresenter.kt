package com.upc.tfm.weoffer.chat

import android.content.Context
import android.content.Intent
import com.upc.tfm.weoffer.home.HomeView
import com.upc.tfm.weoffer.network.ListChatsResponse
import com.upc.tfm.weoffer.profile.OtherView
import com.upc.tfm.weoffer.service.ChatService
import com.upc.tfm.weoffer.utils.UtilUser
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class ChatPresenter(val context: Context, val view: IChatView): ChatService.OnChatCompleted {

    private var chatService: ChatService = ChatService(this)
    private var chat_id: String? = null
    private var user_id: String? = "0"

    fun deleteButtonPressed() {
        view.askDelete()
    }

    fun deleteChat() {
        val token = UtilUser.token
        if (token != null) {
            if (chat_id != null) {
                view.showProgressDialog()
                chatService.deleteChat(token, chat_id!!)
            }
        }
    }

    fun saveChatId(chatId: String?) {
        chat_id = chatId
    }

    fun userClicked() {
        val intent = Intent()
        intent.setClass(context, OtherView::class.java)
        intent.putExtra("user_id", user_id)
        context.startActivity(intent)
    }

    fun setUserId(userId: String?) {
        user_id = userId
    }

    fun updateTime(sender: String, receiver: String) {
        val day = LocalDate.now().dayOfMonth.toString()
        val month = getMonth(LocalDate.now().month.toString())
        val year = LocalDate.now().year.toString()
        val currentDate = LocalDateTime.now()
        val date = day + "/" + month + "/" + year
        val time = currentDate.format(DateTimeFormatter.ofPattern("HH:mm:ss"))
        val token = UtilUser.token
        val request = TimeRequest(
            sender = sender,
            receiver = receiver,
            date = date,
            time = time
        )
        if (token != null)
            chatService.updateTime(token, request)
    }

    private fun getMonth(month: String): String {
        return when (month) {
            "JANUARY" -> "01"
            "FEBRUARY" -> "02"
            "MARCH" -> "03"
            "APRIL" -> "04"
            "MAY" -> "05"
            "JUNE" -> "06"
            "JULY" -> "07"
            "AUGUST" -> "08"
            "SEPTEMBER" -> "09"
            "OCTOBER" -> "10"
            "NOVEMBER" -> "11"
            "DECEMBER" -> "12"
            else -> "00"
        }
    }

    private fun goToListChats() {
        val intent = Intent()
        intent.setClass(context, HomeView::class.java)
        intent.putExtra("view", "3")
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        context.startActivity(intent)
    }

    // Override

    override fun onChatDeleteCompleted(code: Int) {
        view.closeDialog()
        goToListChats()
    }

    override fun onChatCreatedCompleted(code: Int) {}

    override fun onGetListChatsCompleted(response: Pair<Int, ListChatsResponse?>) {}

}