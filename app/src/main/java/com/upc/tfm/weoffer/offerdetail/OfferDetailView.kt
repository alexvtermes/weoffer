package com.upc.tfm.weoffer.offerdetail

import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color.red
import android.graphics.drawable.BitmapDrawable
import android.icu.text.SimpleDateFormat
import android.media.Image
import android.os.Bundle
import android.util.Base64
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.ViewSwitcher
import androidx.appcompat.app.AppCompatActivity
import androidx.core.graphics.drawable.toBitmap
import androidx.core.graphics.drawable.toDrawable
import com.upc.tfm.weoffer.R
import kotlinx.android.synthetic.main.offer_detail_view.*
import kotlinx.android.synthetic.main.offers_item.*
import java.util.*
import java.util.concurrent.TimeUnit

class OfferDetailView: AppCompatActivity(), IOfferDetailView {

    private var presenter: OfferDetailPresenter
    private var dialog: Dialog? = null

    init {
        presenter = OfferDetailPresenter(this, this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.offer_detail_view)
        init()
    }

    private fun init() {
        initToolbar()
        initView()
        initUser()
        initSwitcher()
        initBuyButton()
    }

    private fun initView() {
        val offerId = intent.getStringExtra("offer_id")
        presenter.getOfferDetail(offerId)
    }

    private fun initUser() {
        offer_detail_user_relative.setOnClickListener {
            presenter.userClicked()
        }
    }

    private fun initSwitcher() {
        /*
        image_switcher.setFactory(object: ViewSwitcher.ViewFactory {
            override fun makeView(): View {
                val imageView = ImageView(applicationContext)
                imageView.scaleType = ImageView.ScaleType.CENTER_INSIDE
                return imageView
            }
        })
        */

        next_image_button.setOnClickListener {
            presenter.nextButtonPressed()
        }
        previous_image_button.setOnClickListener {
            presenter.previousButtonPressed()
        }
    }

    private fun initBuyButton() {
        /*buy_button.setOnClickListener {
            presenter.buyButtonPressed()
        }*/
    }

    private fun initToolbar() {
        setSupportActionBar(app_toolbar)
        if (supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        }
    }

    private fun initChatButton() {
        offer_detail_chat_button.setOnClickListener {
            presenter.chatButtonClicked()
        }
    }

    private fun initDeleteButton() {
        if (presenter.isMyOffer()) {
            offer_delete_button.visibility = View.VISIBLE
            offer_delete_button.setOnClickListener {
                presenter.deleteButtonPressed()
            }
            offer_detail_fab.show()
            offer_detail_chat_button.hide()
            initEditButton()
        }
        else {
            offer_delete_button.visibility = View.GONE
            offer_detail_fab.hide()
            offer_detail_chat_button.show()
            initChatButton()
        }
    }

    private fun initEditButton() {
        offer_detail_fab.setOnClickListener {
            presenter.editButtonPressed()
        }
    }

    private fun getImage(image: String?): Bitmap? {
        val encodedString = image
        if (encodedString != null) {
            val encoded = encodedString.split(",")
            val decodedString = Base64.decode(encoded[1], Base64.DEFAULT)
            return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
        }
        return null
    }

    // Override

    override fun showProgressDialog() {
        dialog = Dialog(this)
        dialog?.setContentView(R.layout.progressbar_layout)
        dialog?.setCancelable(false)
        dialog?.setCanceledOnTouchOutside(false)
        dialog?.show()
    }

    override fun closeDialog() {
        dialog?.dismiss()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun handleDate(date: String) {
        val sdf = SimpleDateFormat("dd/MM/yyyy")
        val currentDateString = sdf.format(Date())
        val currentDate = sdf.parse(currentDateString)
        val creationDate = sdf.parse(date)
        val diff = currentDate.time - creationDate.time
        val days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS)
        if (days.toInt() == 0)
            offer_detail_date.text = getString(R.string.posted_today)
        else if (days.toInt() == 1)
            offer_detail_date.text = getString(R.string.posted_yesterday)
        else if (days.toInt() < 7)
            offer_detail_date.text = getString(R.string.posted) + " " + days.toString() + " " + getString(R.string.days_ago)
        else
            offer_detail_date.text = getString(R.string.posted) + " " + date
    }

    override fun showOfferDetail(data: OfferDetailRequest) {
        offer_detail_title.text = data.title
        offer_detail_description.text = data.description
        offer_detail_price.text = data.hour_price.toString()
        if (data.image != null) {
            val image = getImage(data.image)
            offer_detail_image.setImageDrawable(BitmapDrawable(this.resources, image))
            if (data.images.size <= 1) {
                next_image_button.visibility = View.GONE
                previous_image_button.visibility = View.GONE
                offer_images_count.visibility = View.GONE
            }
            else {
                offer_images_count.text = "1 / " + data.images.size.toString()
            }
        }
        offer_detail_person_name.text = data.name_user
        val imageUser = getImage(data.image_user)
        offer_detail_person_image.setImageDrawable(BitmapDrawable(this.resources, imageUser))
        offer_detail_person_image.setImageBitmap(imageUser)
        offer_detail_count.text = data.count.toString()
        if (data.online) {
            offer_detail_symbol_location.setImageDrawable(getDrawable(R.drawable.ic_online))
            offer_detail_location.text = getString(R.string.online)
            offer_detail_location.setTextColor(getColor(R.color.colorRed))
        }
        else {
            offer_detail_location.text = data.location
        }
        handleDate(data.date)
        showStars(data.stars, offer_detail_star_1, offer_detail_star_2, offer_detail_star_3, offer_detail_star_4, offer_detail_star_5)
        initDeleteButton()
    }

    override fun askDelete() {
        val builder1: AlertDialog.Builder = AlertDialog.Builder(this)
        builder1.setMessage(getString(R.string.offer_delete_sure))
        builder1.setCancelable(true)

        builder1.setPositiveButton(
            getString(R.string.yes),
            DialogInterface.OnClickListener { dialog, id ->
                dialog.cancel()
                presenter.deleteOffer()
            })

        builder1.setNegativeButton(
            getString(R.string.no),
            DialogInterface.OnClickListener { dialog, id -> dialog.cancel() })

        val alert11: AlertDialog = builder1.create()
        alert11.show()
    }

    private fun showStars(stars: Float, star_1: ImageView, star_2: ImageView, star_3: ImageView, star_4: ImageView, star_5: ImageView) {
        when (stars) {
            in 4.5..5.0 -> {
                star_1.setBackgroundResource(R.drawable.ic_star_full_2)
                star_2.setBackgroundResource(R.drawable.ic_star_full_2)
                star_3.setBackgroundResource(R.drawable.ic_star_full_2)
                star_4.setBackgroundResource(R.drawable.ic_star_full_2)
                star_5.setBackgroundResource(R.drawable.ic_star_full_2)
            }
            in 3.5..4.5 -> {
                star_1.setBackgroundResource(R.drawable.ic_star_full_2)
                star_2.setBackgroundResource(R.drawable.ic_star_full_2)
                star_3.setBackgroundResource(R.drawable.ic_star_full_2)
                star_4.setBackgroundResource(R.drawable.ic_star_full_2)
                star_5.setBackgroundResource(R.drawable.ic_empty_star)
            }
            in 2.5..3.5 -> {
                star_1.setBackgroundResource(R.drawable.ic_star_full_2)
                star_2.setBackgroundResource(R.drawable.ic_star_full_2)
                star_3.setBackgroundResource(R.drawable.ic_star_full_2)
                star_4.setBackgroundResource(R.drawable.ic_empty_star)
                star_5.setBackgroundResource(R.drawable.ic_empty_star)
            }
            in 1.5..2.5 -> {
                star_1.setBackgroundResource(R.drawable.ic_star_full_2)
                star_2.setBackgroundResource(R.drawable.ic_star_full_2)
                star_3.setBackgroundResource(R.drawable.ic_empty_star)
                star_4.setBackgroundResource(R.drawable.ic_empty_star)
                star_5.setBackgroundResource(R.drawable.ic_empty_star)
            }
            in 0.5..1.5 -> {
                star_1.setBackgroundResource(R.drawable.ic_star_full_2)
                star_2.setBackgroundResource(R.drawable.ic_empty_star)
                star_3.setBackgroundResource(R.drawable.ic_empty_star)
                star_4.setBackgroundResource(R.drawable.ic_empty_star)
                star_5.setBackgroundResource(R.drawable.ic_empty_star)
            }
            else -> {
                star_1.setBackgroundResource(R.drawable.ic_empty_star)
                star_2.setBackgroundResource(R.drawable.ic_empty_star)
                star_3.setBackgroundResource(R.drawable.ic_empty_star)
                star_4.setBackgroundResource(R.drawable.ic_empty_star)
                star_5.setBackgroundResource(R.drawable.ic_empty_star)
            }
        }
    }

    override fun showImage(image: String) {
        offer_detail_image.setImageDrawable(BitmapDrawable(this.resources, getImage(image)))
    }

    override fun updateCount(count: Int, size: Int) {
        offer_images_count.text = count.toString() + " / " + size.toString()
    }

}