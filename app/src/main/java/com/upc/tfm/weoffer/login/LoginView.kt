package com.upc.tfm.weoffer.login

import android.app.Dialog
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.upc.tfm.weoffer.R
import kotlinx.android.synthetic.main.login_view.*

class LoginView: AppCompatActivity(), ILoginView {

    private var presenter: LoginPresenter = LoginPresenter(this, this)
    private var dialog: Dialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login_view)
        init()
    }

    private fun init() {
        initLoginButton()
        initRegisterButton()
    }

    private fun initLoginButton() {
        login_button.setOnClickListener {
            presenter.loginButtonPressed(login_email.text.toString(), login_password.text.toString())
        }
    }

    private fun initRegisterButton() {
        login_register.setOnClickListener {
            presenter.registerButtonPressed()
        }
    }

    private fun createToast(msg: String) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }

    // Override

    override fun showEmptyEmail() {
        login_email.error = getString(R.string.login_email_miss)
    }

    override fun showEmptyPassword() {
        login_password.error = getString(R.string.login_password_miss)
    }

    override fun showEmailNotValid() {
        login_email.error = getString(R.string.login_email_not_valid)
    }

    override fun showToast(text: String) {
        createToast(text)
    }

    override fun showProgressDialog() {
        dialog = Dialog(this)
        dialog?.setContentView(R.layout.progressbar_layout)
        dialog?.setCancelable(false)
        dialog?.setCanceledOnTouchOutside(false)
        dialog?.show()
    }

    override fun closeDialog() {
        dialog?.dismiss()
    }

}
