package com.upc.tfm.weoffer.register

interface IRegisterView {

    fun showMissingName()
    fun showMissingSurname()
    fun showMissingDate()
    fun showMissingCountry()
    fun showMissingPassword1()
    fun showMissingPassword2()
    fun showMissingEmail()
    fun showMissingFields()
    fun showEmailNotValid()
    fun showPasswordsNotMatch()
    fun showToast(text: String)
    fun showProgressDialog()
    fun closeDialog()

}