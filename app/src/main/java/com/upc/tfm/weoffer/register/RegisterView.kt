package com.upc.tfm.weoffer.register

import android.app.Activity
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.upc.tfm.weoffer.R
import kotlinx.android.synthetic.main.offer_detail_view.*
import kotlinx.android.synthetic.main.register_view.*
import java.io.InputStream
import java.util.*

class RegisterView: AppCompatActivity(), IRegisterView {

    private var presenter: RegisterPresenter = RegisterPresenter(this, this)
    private var dialog: Dialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.register_view)
        init()
    }

    private fun init() {
        initToolbar()
        initRegisterButton()
        initCountries()
        initDatePicker()
        initSpinnerGender()
        initImage()
    }

    private fun initRegisterButton() {
        register_button.setOnClickListener {
            presenter.registerButtonPressed(
                registerName = register_name.text.toString(),
                registerSurname = register_surnames.text.toString(),
                registerGender = register_gender.selectedItem.toString(),
                registerDate = register_date_button.text.toString(),
                registerCountry = register_country.text.toString(),
                registerEmail = register_email.text.toString(),
                registerPassword1 = register_password_1.text.toString(),
                registerPassword2 = register_password_2.text.toString(),
                registerImage = drawableToBitmap(register_image.drawable)
            )
        }
    }

    private fun initToolbar() {
        setSupportActionBar(app_toolbar)
        if (supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        }
    }

    private fun initImage() {
        register_image.setOnClickListener {
            presenter.imageClicked()
        }
    }

    private fun initCountries() {
        register_country.setAdapter(
            ArrayAdapter<String>(
            this,
            R.layout.spinner,
            presenter.getCountries()
        )
        )
        register_country.threshold = 1
    }

    private fun initDatePicker() {
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)
        var birthMonth = month.toInt() + 1
        var date = "" + day + "/" + birthMonth.toString() + "/" + year
        register_date_button.text = date

        register_date_button.setOnClickListener {

            val dpd = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                birthMonth = monthOfYear.toInt() + 1
                date = "" + dayOfMonth + "/" + birthMonth.toString() + "/" + year
                register_date_button.text = date
            }, year, month, day)

            dpd.show()
        }
    }

    private fun initSpinnerGender() {
        var adapter = ArrayAdapter<String>(
            this,
            R.layout.spinner,
            presenter.getGenders())
        adapter.setDropDownViewResource(R.layout.spinner)
        register_gender.adapter = adapter

        register_gender.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                //presenter.genderTypeSelected(position)
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                //Nothing
            }
        }
    }

    private fun createToast(msg: String) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }

    private fun drawableToBitmap(drawable: Drawable): Bitmap? {
        var bitmap: Bitmap? = null

        if (drawable is BitmapDrawable) {
            if (drawable.bitmap != null) {
                return drawable.bitmap
            }
        }

        if (drawable.intrinsicWidth <= 0 || drawable.intrinsicHeight <= 0) {
            bitmap = Bitmap.createBitmap(
                1,
                1,
                Bitmap.Config.ARGB_8888
            )
        } else {
            bitmap = Bitmap.createBitmap(drawable.intrinsicWidth, drawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
        }

        val canvas = Canvas(bitmap)
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight())
        drawable.draw(canvas)
        return bitmap
    }

    // Override

    override fun showMissingCountry() {
        register_country.error = getString(R.string.register_missing_country)
    }

    override fun showMissingDate() {
        register_date_button.error = getString(R.string.register_missing_date)
    }

    override fun showMissingEmail() {
        register_email.error = getString(R.string.register_missing_email)
    }

    override fun showMissingName() {
        register_name.error = getString(R.string.register_missing_name)
    }

    override fun showMissingPassword1() {
        register_password_1.error = getString(R.string.register_missing_password)
    }

    override fun showMissingPassword2() {
        register_password_2.error = getString(R.string.register_missing_password)
    }

    override fun showMissingSurname() {
        register_surnames.error = getString(R.string.register_missing_surname)
    }

    override fun showMissingFields() {
        createToast(getString(R.string.register_missing_fields))
    }

    override fun showEmailNotValid() {
        register_email.error = getString(R.string.register_email_not_valid)
        createToast(getString(R.string.register_email_not_valid))
    }

    override fun showPasswordsNotMatch() {
        createToast(getString(R.string.register_passwords_not_match))
    }

    override fun showToast(text: String) {
        createToast(text)
    }

    override fun showProgressDialog() {
        dialog = Dialog(this)
        dialog?.setContentView(R.layout.progressbar_layout)
        dialog?.setCancelable(false)
        dialog?.setCanceledOnTouchOutside(false)
        dialog?.show()
    }

    override fun closeDialog() {
        dialog?.dismiss()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            try {
                val imageUri = data?.data
                if (imageUri != null) {
                    val imageStream: InputStream? = contentResolver.openInputStream(imageUri)
                    val imageBitmap: Bitmap = BitmapFactory.decodeStream(imageStream)
                    register_image.setImageBitmap(imageBitmap)
                }
            } catch (exception: Exception) {
                Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show();
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

}