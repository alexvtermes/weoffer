package com.upc.tfm.weoffer.offers

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.icu.text.SimpleDateFormat
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.upc.tfm.weoffer.R
import com.upc.tfm.weoffer.searchoffer.SearchOfferRequest
import kotlinx.android.synthetic.main.offer_detail_view.*
import kotlinx.android.synthetic.main.offers_item.view.*
import kotlinx.android.synthetic.main.rate_user_view.view.*
import java.util.*
import java.util.concurrent.TimeUnit

class OffersAdapter(val context: Context, var listOffers: List<OfferRequest>, val presenter: OffersPresenter) : RecyclerView.Adapter<ViewHolder>() {

    fun getImage(image: String?): Bitmap? {
        val encodedString = image
        if (encodedString != null) {
            val encoded = encodedString.split(",")
            val decodedString = Base64.decode(encoded[1], Base64.DEFAULT)
            return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
        }
        return null
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.title?.text = listOffers[position].title
        holder.price?.text = listOffers[position].hour_price
        holder.user_name?.text = listOffers[position].user_name
        holder.user_count?.text = listOffers[position].user_count.toString()
        if (listOffers[position].online) {
            holder.online.visibility = View.VISIBLE
        }
        else {
            holder.online.visibility = View.GONE
        }
        showStars(listOffers[position].user_stars, holder.user_star_1, holder.user_star_2, holder.user_star_3, holder.user_star_4, holder.user_star_5)
        if (listOffers[position].image == null) {
            holder.image.setBackgroundResource(R.drawable.no_image)
        }
        else {
            val image = getImage(listOffers[position].image)
            if (image != null) {
                holder.image?.setImageDrawable(BitmapDrawable(context.resources, image))
                holder.image?.setImageBitmap(image)
            }
        }
        if (listOffers[position].user_image == null) {
            holder.user_image.setBackgroundResource(R.drawable.ic_person)
        }
        else {
            val userImage = getImage(listOffers[position].user_image)
            if (userImage != null) {
                holder.user_image?.setImageDrawable(BitmapDrawable(context.resources, userImage))
                holder.user_image?.setImageBitmap(userImage)
            }
        }
        handleDate(holder, listOffers[position].date)
    }

    private fun handleDate(holder: ViewHolder, date: String) {
        val sdf = SimpleDateFormat("dd/MM/yyyy")
        val currentDateString = sdf.format(Date())
        val currentDate = sdf.parse(currentDateString)
        val creationDate = sdf.parse(date)
        val diff = currentDate.time - creationDate.time
        val days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS)
        if (days.toInt() == 0)
            holder.date.text = context.getString(R.string.posted_today)
        else if (days.toInt() == 1)
            holder.date.text = context.getString(R.string.posted_yesterday)
        else if (days.toInt() < 7)
            holder.date.text = days.toString() + " " + context.getString(R.string.days_ago)
        else
            holder.date.text = date
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.offers_item, parent, false))
    }

    override fun getItemCount(): Int {
        return listOffers.size
    }
    private fun showStars(stars: Float, star_1: ImageView, star_2: ImageView, star_3: ImageView, star_4: ImageView, star_5: ImageView) {
        when (stars) {
            in 4.5..5.0 -> {
                star_1.setBackgroundResource(R.drawable.ic_star_full_2)
                star_2.setBackgroundResource(R.drawable.ic_star_full_2)
                star_3.setBackgroundResource(R.drawable.ic_star_full_2)
                star_4.setBackgroundResource(R.drawable.ic_star_full_2)
                star_5.setBackgroundResource(R.drawable.ic_star_full_2)
            }
            in 3.5..4.5 -> {
                star_1.setBackgroundResource(R.drawable.ic_star_full_2)
                star_2.setBackgroundResource(R.drawable.ic_star_full_2)
                star_3.setBackgroundResource(R.drawable.ic_star_full_2)
                star_4.setBackgroundResource(R.drawable.ic_star_full_2)
                star_5.setBackgroundResource(R.drawable.ic_empty_star)
            }
            in 2.5..3.5 -> {
                star_1.setBackgroundResource(R.drawable.ic_star_full_2)
                star_2.setBackgroundResource(R.drawable.ic_star_full_2)
                star_3.setBackgroundResource(R.drawable.ic_star_full_2)
                star_4.setBackgroundResource(R.drawable.ic_empty_star)
                star_5.setBackgroundResource(R.drawable.ic_empty_star)
            }
            in 1.5..2.5 -> {
                star_1.setBackgroundResource(R.drawable.ic_star_full_2)
                star_2.setBackgroundResource(R.drawable.ic_star_full_2)
                star_3.setBackgroundResource(R.drawable.ic_empty_star)
                star_4.setBackgroundResource(R.drawable.ic_empty_star)
                star_5.setBackgroundResource(R.drawable.ic_empty_star)
            }
            in 0.5..1.5 -> {
                star_1.setBackgroundResource(R.drawable.ic_star_full_2)
                star_2.setBackgroundResource(R.drawable.ic_empty_star)
                star_3.setBackgroundResource(R.drawable.ic_empty_star)
                star_4.setBackgroundResource(R.drawable.ic_empty_star)
                star_5.setBackgroundResource(R.drawable.ic_empty_star)
            }
            else -> {
                star_1.setBackgroundResource(R.drawable.ic_empty_star)
                star_2.setBackgroundResource(R.drawable.ic_empty_star)
                star_3.setBackgroundResource(R.drawable.ic_empty_star)
                star_4.setBackgroundResource(R.drawable.ic_empty_star)
                star_5.setBackgroundResource(R.drawable.ic_empty_star)
            }
        }
    }

    fun updateData(data: List<OfferRequest>) {
        listOffers += data
        //presenter.updateListOffers(listOffers)
        notifyDataSetChanged()
    }


}

class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {
    val title = view.offer_title
    val price = view.offer_price
    val image = view.offer_image
    val user_image = view.offer_user_image
    val user_name = view.offer_user_name
    val user_star_1 = view.offer_star_1
    val user_star_2 = view.offer_star_2
    val user_star_3 = view.offer_star_3
    val user_star_4 = view.offer_star_4
    val user_star_5 = view.offer_star_5
    val user_count = view.user_review_count
    val online = view.offer_detail_online_icon
    val date = view.offer_item_date
}

interface OnItemClickListener {
    fun onItemClicked(position:Int, view: View)
}


fun RecyclerView.addOnItemClickListener(onClickListener: OnItemClickListener) {
    this.addOnChildAttachStateChangeListener(object: RecyclerView.OnChildAttachStateChangeListener {
        override fun onChildViewDetachedFromWindow(view: View) {
            view.setOnClickListener(null)
        }

        override fun onChildViewAttachedToWindow(view: View) {
            view.setOnClickListener({
                val holder = getChildViewHolder(view)
                onClickListener.onItemClicked(holder.adapterPosition, view)
            })
        }
    })
}