package com.upc.tfm.weoffer.profile.offers

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.upc.tfm.weoffer.R
import com.upc.tfm.weoffer.network.MyOffersResponse
import com.upc.tfm.weoffer.profile.ProfilePresenter
import kotlinx.android.synthetic.main.profile_offer_fragment.*

class ProfileOffersFragment: Fragment() {

    private lateinit var itemAdapter: ProfileOffersAdapter
    private var other: Boolean = false


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.profile_offer_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val bundle = arguments
        if (bundle != null) {
            val list: MyOffersResponse = bundle.getSerializable("Object") as MyOffersResponse
            initRecyclerView(list)
        }
    }

    private fun initRecyclerView(list: MyOffersResponse?) {
        if (list != null) {
            if (recycler_offers != null) {
                recycler_offers.layoutManager = LinearLayoutManager(context)
                itemAdapter = ProfileOffersAdapter(context, list)
                recycler_offers.adapter = itemAdapter
                if (list.data.isEmpty()) {
                    profile_empty_view.visibility = View.VISIBLE
                }
                else profile_empty_view.visibility = View.GONE
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
    }

}