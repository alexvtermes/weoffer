package com.upc.tfm.weoffer.createoffer

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.media.Image
import android.provider.MediaStore
import android.util.Base64
import androidx.core.app.ActivityCompat
import com.upc.tfm.weoffer.home.HomeView
import com.upc.tfm.weoffer.network.ImagesResponse
import com.upc.tfm.weoffer.network.MyOffersResponse
import com.upc.tfm.weoffer.network.OfferDetailResponse
import com.upc.tfm.weoffer.network.OffersResponse
import com.upc.tfm.weoffer.offerdetail.ImageRequest
import com.upc.tfm.weoffer.register.RegisterRequest
import com.upc.tfm.weoffer.service.OfferService
import com.upc.tfm.weoffer.utils.UtilUser
import java.io.ByteArrayOutputStream

class CreateOfferPresenter(val context: Context, val view: ICreateOfferView): OfferService.OnOfferCompleted  {

    private var offerService: OfferService = OfferService(this)
    private var offerId: String? = null
    private var array_images: ArrayList<Bitmap?> = ArrayList()


    fun createOfferButtonPressed(title: String, description: String, online: Boolean, location: String,
                                hour_total: Boolean, hour_price: String, total_price: String) {
        if (!checkRequiredFields(title, description, online, location, hour_total, hour_price, total_price)) {
            createOffer(title, description, online, location, hour_price, total_price)
        }
        else
            view.showMissingFields()
    }

    private fun checkRequiredFields(title: String, description: String, online: Boolean, location: String,
                            hour_total: Boolean, hour_price: String, total_price: String): Boolean {
        var miss = false
        if (title.isEmpty()) {
            view.showMissingTitle()
            miss = true
        }
        if (description.isEmpty()) {
            view.showMissingDescription()
            miss = true
        }
        if (!online) {
            if (location.isEmpty()) {
                view.showMissingLocation()
                miss = true
            }
        }
        if (hour_total) {
            if (hour_price.isEmpty()) {
                view.showMissingHourPrice()
                miss = true
            }
        }
        else {
            if (total_price.isEmpty()) {
                view.showMissingTotalPrice()
                miss = true
            }
        }
        return miss
    }

    fun addImage(image: Bitmap?) {
        array_images.add(image)
    }

    fun deleteImage(image: Bitmap?) {
        array_images.remove(image)
    }

    fun editOfferButtonPressed(title: String, description: String, online: Boolean, location: String,
                                 hour_total: Boolean, hour_price: String, total_price: String) {
        if (!checkRequiredFields(title, description, online, location, hour_total, hour_price, total_price)) {
            editOffer(title, description, online, location, hour_price, total_price)
        }
        else
            view.showMissingFields()
    }

    private fun encodeImage(image: Bitmap?): ImageRequest? {
        if (image != null) {
            val bos = ByteArrayOutputStream()
            image?.compress(Bitmap.CompressFormat.JPEG, 50, bos)
            var encodedImage = Base64.encodeToString(bos.toByteArray(), Base64.NO_WRAP)
            encodedImage = "data:image/png;base64," + encodedImage
            val request = ImageRequest(
                id = null,
                offer_id = null,
                image = encodedImage
            )
            return request
        }
        return null
    }

    private fun createOffer(title: String, description: String, online: Boolean, location: String,
                            hour_price: String, total_price: String) {
        val encodedImages = ArrayList<ImageRequest?>()
        for (image in array_images) {
            encodedImages.add(encodeImage(image))
        }
        val request = CreateOfferRequest(
            title = title,
            description = description,
            online = online,
            location = location,
            hour_price = hour_price.toFloat(),
            total_price = total_price.toFloat(),
            images = encodedImages
        )
        val token = UtilUser.token
        if (token != null) {
            view.showProgressDialog()
            offerService.createOffer(request, token)
        }
    }

    private fun editOffer(title: String, description: String, online: Boolean, location: String,
                            hour_price: String, total_price: String) {
        val encodedImages = ArrayList<ImageRequest?>()
        for (image in array_images) {
            encodedImages.add(encodeImage(image))
        }
        val request = CreateOfferRequest(
            title = title,
            description = description,
            online = online,
            location = location,
            hour_price = hour_price.toFloat(),
            total_price = total_price.toFloat(),
            images = encodedImages
        )
        val token = UtilUser.token
        if (token != null && offerId != null) {
            view.showProgressDialog()
            offerService.editOffer(request, token, offerId!!)
        }
    }

    private fun handleResponse(code: Int) {
        when (code) {
            in 200..299 -> {
                goToMyOffersView()
            }
            else -> {
                view.showServerError()
            }
        }
    }

    private fun goToMyOffersView() {
        /*val intent = Intent()
        intent.setClass(context, HomeView::class.java)
        intent.putExtra("view", "2")
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        context.startActivity(intent)
         */
        view.finishCreate()
    }

    fun setOfferId(offer_id: String?) {
        offerId = offer_id
    }

    fun getImages(offer_id: String?) {
        if (offer_id != null) {
            val token = UtilUser.token
            if (token != null)
                offerService.getImages(token, offer_id)
        }
    }

    fun addPhotoButtonPressed() {
        val galleryIntent = Intent(
            Intent.ACTION_PICK,
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        )
        ActivityCompat.startActivityForResult(
            context as Activity,
            galleryIntent,
            1,
            null
        )
    }

    private fun handleImagesResponse(code: Int, data: ImagesResponse?) {
        when (code) {
            in 200..299 -> {
                if (data != null) {
                    view.showImages(data.data)
                }
            }
        }
    }

    // Override

    override fun onOfferCreatedCompleted(code: Int) {
        view.closeDialog()
        handleResponse(code)
    }

    override fun onImagesCompleted(response: Pair<Int, ImagesResponse?>) {
        handleImagesResponse(response.first, response.second)
    }

    override fun onMyOffersCompleted(response: Pair<Int, MyOffersResponse?>) {}

    override fun onOfferDetailCompleted(response: Pair<Int, OfferDetailResponse?>) {}

    override fun onOffersCompleted(response: Pair<Int, OffersResponse?>) {}

    override fun onDeleteOfferCompleted(code: Int) {}

}