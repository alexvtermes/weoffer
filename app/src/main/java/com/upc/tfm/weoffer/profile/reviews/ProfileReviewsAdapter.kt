package com.upc.tfm.weoffer.profile.reviews

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.upc.tfm.weoffer.R
import com.upc.tfm.weoffer.network.MyOffersResponse
import com.upc.tfm.weoffer.network.ReviewsResponse
import com.upc.tfm.weoffer.rateuser.ReviewResponse
import kotlinx.android.synthetic.main.profile_offer_item.view.*
import kotlinx.android.synthetic.main.profile_review_item.view.*
import kotlinx.android.synthetic.main.rate_user_view.*

class ProfileReviewsAdapter(val context: Context?, var list: ReviewsResponse): RecyclerView.Adapter<ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.user_name.text = list.data[position].sender_name
        holder.message.text = list.data[position].message
        holder.date.text = list.data[position].date
        showStars(list.data[position].stars, holder.star_1, holder.star_2, holder.star_3, holder.star_4, holder.star_5)
        if (list.data[position].sender_image == null) {
            holder.user_image.setBackgroundResource(R.drawable.ic_person)
        }
        else {
            val userImage = getImage(list.data[position].sender_image)
            if (userImage != null) {
                holder.user_image?.setImageDrawable(BitmapDrawable(context?.resources, userImage))
                holder.user_image?.setImageBitmap(userImage)
            }
        }
    }

    private fun getImage(image: String?): Bitmap? {
        val encodedString = image
        if (encodedString != null) {
            val encoded = encodedString.split(",")
            val decodedString = Base64.decode(encoded[1], Base64.DEFAULT)
            return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
        }
        return null
    }

    private fun showStars(stars: Int, star_1: ImageView, star_2: ImageView, star_3: ImageView, star_4: ImageView, star_5: ImageView) {
        when (stars) {
            5 -> {
                star_1.setBackgroundResource(R.drawable.ic_star_full_2)
                star_2.setBackgroundResource(R.drawable.ic_star_full_2)
                star_3.setBackgroundResource(R.drawable.ic_star_full_2)
                star_4.setBackgroundResource(R.drawable.ic_star_full_2)
                star_5.setBackgroundResource(R.drawable.ic_star_full_2)
            }
            4 -> {
                star_1.setBackgroundResource(R.drawable.ic_star_full_2)
                star_2.setBackgroundResource(R.drawable.ic_star_full_2)
                star_3.setBackgroundResource(R.drawable.ic_star_full_2)
                star_4.setBackgroundResource(R.drawable.ic_star_full_2)
                star_5.setBackgroundResource(R.drawable.ic_empty_star)
            }
            3 -> {
                star_1.setBackgroundResource(R.drawable.ic_star_full_2)
                star_2.setBackgroundResource(R.drawable.ic_star_full_2)
                star_3.setBackgroundResource(R.drawable.ic_star_full_2)
                star_4.setBackgroundResource(R.drawable.ic_empty_star)
                star_5.setBackgroundResource(R.drawable.ic_empty_star)
            }
            2 -> {
                star_1.setBackgroundResource(R.drawable.ic_star_full_2)
                star_2.setBackgroundResource(R.drawable.ic_star_full_2)
                star_3.setBackgroundResource(R.drawable.ic_empty_star)
                star_4.setBackgroundResource(R.drawable.ic_empty_star)
                star_5.setBackgroundResource(R.drawable.ic_empty_star)
            }
            1 -> {
                star_1.setBackgroundResource(R.drawable.ic_star_full_2)
                star_2.setBackgroundResource(R.drawable.ic_empty_star)
                star_3.setBackgroundResource(R.drawable.ic_empty_star)
                star_4.setBackgroundResource(R.drawable.ic_empty_star)
                star_5.setBackgroundResource(R.drawable.ic_empty_star)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.profile_review_item,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.data.size
    }

    fun updateData(newData: ReviewsResponse) {
        list = newData
    }

}


class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {
    val user_image = view.review_user_image
    val user_name = view.review_user_name
    val message = view.review_message
    val date = view.review_date
    val star_1 = view.review_star_1
    val star_2 = view.review_star_2
    val star_3 = view.review_star_3
    val star_4 = view.review_star_4
    val star_5 = view.review_star_5
}