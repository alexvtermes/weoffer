package com.upc.tfm.weoffer.offers


interface IOffersView {

    fun showProgressDialog()
    fun closeDialog()
    fun setOffers(data: List<OfferRequest>)
    fun updateOffers(data: List<OfferRequest>?)
    fun checkEmpty(list: List<OfferRequest>?)

}