package com.upc.tfm.weoffer.chat

import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.util.Base64
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import com.upc.tfm.weoffer.R
import com.upc.tfm.weoffer.utils.UtilUser
import kotlinx.android.synthetic.main.chat_view.*
import kotlinx.android.synthetic.main.message.view.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class ChatView: AppCompatActivity(), IChatView {

    private var userId: String? = ""
    private var adapter: FirebaseRecyclerAdapter<MessageAdapter, MessageAdapter.ViewHolder>? = null
    private var fuser: FirebaseUser? = null
    private lateinit var reference: DatabaseReference
    private lateinit var listchat: ArrayList<Chat>
    private lateinit var messageAdapter: MessageAdapter
    private lateinit var presenter: ChatPresenter
    private var dialog: Dialog? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        init()
    }

    private fun init() {
        setContentView(R.layout.chat_view)
        presenter = ChatPresenter(this,this)
        initToolbar()
        initUserInfo()
        prepareRecyclerView()
        prepareFirebase()
        initFloatingButton()
        initDeleteButton()
        initProfileButton()
    }

    private fun initToolbar() {
        setSupportActionBar(app_bar_chat)
        if (supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun initUserInfo() {
        userId = intent.getStringExtra("userId")
        presenter.setUserId(userId)
        val userName = intent.getStringExtra("userName")
        val userImage = intent.getStringExtra("userImage")
        val chatId = intent.getStringExtra("chatId")
        if (chatId == "0") {
            chat_delete_button.visibility = View.GONE
        }
        else {
            chat_delete_button.visibility = View.VISIBLE
            presenter.saveChatId(chatId)
        }
        chat_name.text = userName
        val image = getUserImage(userImage)
        chat_image.setImageDrawable(BitmapDrawable(resources, image))
        chat_image.setImageBitmap(image)
    }

    private fun getUserImage(image: String?): Bitmap? {
        val encodedString = image
        if (encodedString != null) {
            val encoded = encodedString.split(",")
            val decodedString = Base64.decode(encoded[1], Base64.DEFAULT)
            return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
        }
        return null
    }

    private fun prepareFirebase() {
        fuser = FirebaseAuth.getInstance().currentUser
        if (userId != null) {
            reference = FirebaseDatabase.getInstance().getReference("Users").child(userId!!)
            reference.addValueEventListener(object : ValueEventListener {
                override fun onDataChange(p0: DataSnapshot) {
                    if (userId != null) {
                        readMessages(UtilUser.id!!, userId!!)
                    }
                }

                override fun onCancelled(p0: DatabaseError) {
                }
            })
        }
    }

    private fun initDeleteButton() {
        chat_delete_button.setOnClickListener {
            presenter.deleteButtonPressed()
        }
    }

    private fun initProfileButton() {
        chat_image.setOnClickListener {
            presenter.userClicked()
        }
        chat_name.setOnClickListener {
            presenter.userClicked()
        }
    }

    private fun prepareRecyclerView() {
        chat_recyclerView.setHasFixedSize(true)
        val linearLayoutManager = LinearLayoutManager(this)
        linearLayoutManager.stackFromEnd = true
        chat_recyclerView.layoutManager = linearLayoutManager
    }

    private fun readMessages(myId: String, userId: String) {
        listchat = ArrayList<Chat>()
        reference = FirebaseDatabase.getInstance().getReference("Chats")
        reference.addValueEventListener(object: ValueEventListener {

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                listchat.clear()
                for (data in dataSnapshot.children) {
                    val chat: Chat? = data.getValue(Chat::class.java)
                    if ((chat!!.receiver == myId && chat.sender == userId) || (chat.receiver == userId && chat.sender == myId)) {
                        listchat.add(chat)
                    }
                }
                messageAdapter = MessageAdapter(this@ChatView, listchat)
                chat_recyclerView.adapter = messageAdapter
            }

            override fun onCancelled(dataSnapshot: DatabaseError) {
            }
        })
    }

    private fun initFloatingButton() {
        fab_chat.setOnClickListener {
            if (message_box.text.toString() != "") {
                if (userId != null) {
                    sendMessage(message_box.text.toString(), UtilUser.id!!, userId!!)
                    message_box.setText("")
                }
            }
        }
    }

    private fun sendMessage(msg: String, sender: String, receiver: String) {
        val ref: DatabaseReference = FirebaseDatabase.getInstance().reference
        val map = HashMap<String, String>()
        map.put("sender", sender)
        map.put("receiver", receiver)
        map.put("message", msg)
        val format = SimpleDateFormat("HH:mm", Locale.US)
        val time = format.format(Date())
        map.put("time", time)
        ref.child("Chats").push().setValue(map)
        presenter.updateTime(sender, receiver)
    }

    // Override

    override fun askDelete() {
        val builder1: AlertDialog.Builder = AlertDialog.Builder(this)
        builder1.setMessage(getString(R.string.chat_delete_sure))
        builder1.setCancelable(true)

        builder1.setPositiveButton(
            getString(R.string.yes),
            DialogInterface.OnClickListener { dialog, id ->
                dialog.cancel()
                presenter.deleteChat()
            })

        builder1.setNegativeButton(
            getString(R.string.no),
            DialogInterface.OnClickListener { dialog, id -> dialog.cancel() })

        val alert11: AlertDialog = builder1.create()
        alert11.show()
    }

    override fun showProgressDialog() {
        dialog = Dialog(this)
        dialog?.setContentView(R.layout.progressbar_layout)
        dialog?.setCancelable(false)
        dialog?.setCanceledOnTouchOutside(false)
        dialog?.show()
    }

    override fun closeDialog() {
        dialog?.dismiss()
    }

}


class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var message = itemView.message_text
    var user = itemView.message_user
    var date = itemView.message_time

    fun setMessage(msg: String?) {
        message.text = msg
    }

    fun setUser(user: String?) {
        this.user.text = user
    }
}