package com.upc.tfm.weoffer.listchats

import com.upc.tfm.weoffer.network.ListChatsResponse

interface IListChatsView {

    fun showProgressDialog()
    fun closeDialog()
    fun setChats(data: List<ListChatsRequest>)

}