package com.upc.tfm.weoffer.buy

import android.app.Dialog
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.upc.tfm.weoffer.R

class BuyView: AppCompatActivity(), IBuyView {

    private var presenter: BuyPresenter = BuyPresenter(this, this)
    private var dialog: Dialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.buy_view)
        init()
    }

    private fun init() {

    }

}