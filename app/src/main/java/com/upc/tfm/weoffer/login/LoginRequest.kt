package com.upc.tfm.weoffer.login

data class LoginRequest (
    val email: String,
    val password: String
)

data class LoginResponse (
    val id: String,
    val name: String,
    val surname: String,
    val email: String,
    val gender: String,
    val birthday: String,
    val country: String,
    val token: String,
    val image: String?,
    val stars: Float?,
    val count: Int?
)

data class ProfileImageRequest (
    val image: String
)